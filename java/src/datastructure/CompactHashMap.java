package datastructure;

/*
* Memory efficient hash map compared to the provided HashMap
* TODO through testing and bug fixing
*/

public class CompactHashMap {
	// int so consumes less memory
	// var to speed up size lookup
	private int _bucketSize;
	private int _valueBytes;
	// 12 byte overhead (24 bytes for 64bit) + (number of primitives * size)
	private int[] _tableKeys;
	private byte[] _tableValues;
	
	private int _collisions = 0;
	private int _failedRelocations = 0;
	
	public CompactHashMap(int bucketSize, int valueBytes) {
		_bucketSize = bucketSize;
		_valueBytes = valueBytes;
		_tableKeys = new int[_bucketSize];
		_tableValues = new byte[_bucketSize*_valueBytes];
	}
	
	public void put(byte[] keyBytes, byte[] value) {
		int key = hashKey(keyBytes);
		put(key, value);
	}
	
	public void put(int key, byte[] value) {
		boolean failedRelocation = true;
		int index = index(key);
		int[] keys = _tableKeys;
		int i = index;
		int j = index;
		do {
			if(i < _bucketSize && (_tableKeys[i] == 0 || _tableKeys[i] == key)) {
				_tableKeys[i] = key;
				putValue(value, i);
				failedRelocation = false;
				break;
			}
			else if(j >= 0 && (_tableKeys[j] == 0 || _tableKeys[j] == key)) {
				_tableKeys[j] = key;
				putValue(value, j);
				failedRelocation = false;
				break;
			}
			i++;
			j--;
			_collisions++;
		}
		while(i < _bucketSize || j >= 0);
		
		if(failedRelocation) {
			_failedRelocations++;
		}
	}
	
	public boolean containsKey(byte[] keyBytes) {
		boolean containsKey = false;
		int key = hashKey(keyBytes);
		int index = index(key);
		int i = index;
		int j = index;
		do {
			// one side might be out of bounds first
			if(i < _bucketSize) {
				if(_tableKeys[i] == key) {
					containsKey = true;
					break;
				}
				// can't put in while clause in case out of bounds
				else if(_tableKeys[i] == 0) {
					break;
				}
			}
			else if(j >= 0) {
				if(_tableKeys[j] == key) {
					containsKey = true;
					break;
				}
				else if(_tableKeys[j] == 0) {
					break;
				}
			}
			i++;
			j--;
		}
		while(i < _bucketSize || j >= 0);
		return containsKey;
	}
	
	public byte[] get(byte[] keyBytes) {
		int key = hashKey(keyBytes);
		return get(key);
	}
	
	public byte[] get(int key) {
		byte[] value = null;	
		int index = index(key);
		int i = index;
		int j = index;
		
		do {
			// one side might be out of bounds first
			if(i < _bucketSize) {
				if(_tableKeys[i] == key) {
					value = getValue(i);
					break;
				}
				// can't put in while clause in case out of bounds
				else if(_tableKeys[i] == 0) {
					break;
				}
			}
			else if(j >= 0) {
				if(_tableKeys[j] == key) {
					value = getValue(j);
					break;
				}
				else if(_tableKeys[j] == 0) {
					break;
				}
			}
			i++;
			j--;
		}
		while(i < _bucketSize || j >= 0);
		
		return value;
	}
	
	private void putValue(byte[] value, int index) {
		byte[] values = _tableValues;
		int valueIndex = index * _valueBytes;
		for(int i = 0; i < _valueBytes; i++) {
			values[valueIndex+i] = value[i];
		}
	}
	
	private byte[] getValue(int index) {
		byte[] values = _tableValues;
		byte[] value = new byte[_valueBytes];
		int valueIndex = index * _valueBytes;
		for(int i = 0; i < _valueBytes; i++) {
			value[i] = values[valueIndex+i];
		}
		return value;
	}
	
	private int index(int key) {
		int index = key % _bucketSize;
		return index > 0 ? index : index * -1;
	}
	
	public int size() {
		int[] keys = _tableKeys;
		int count = 0;
		for(int i = 0; i < keys.length; i++) {
			if(keys[i] != 0) {
				count++;
			}
		}
		return count;
	}
	
	public int capacity() {
		return _bucketSize;
	}
	
	public float load() {
		return size() / (float)capacity();
	}
	
	private int hashKey(byte[] key) {
		return hash(key);
	}
	
	public void printStats() {
		StringBuffer stats = new StringBuffer();
		stats.append("Collisions: ").append(_collisions).append("\n");
		stats.append("Relocation failures: ").append(_failedRelocations).append("\n");
		int[] keys = _tableKeys;
		stats.append("Table ").append("count: ").append(size()).append("/").append(capacity()).append("\n");
		stats.append("Load: ").append(load()).append("\n");
		System.out.println(stats.toString());
	}
	
	private int hash(byte[] data) {
		final int PRIME = 19;
		int hash = 0;
		for(int i = 0; i < data.length; i++) {
			hash = hash + ((int)data[i] * pow(PRIME, i));
		}
		return hash;
	}
	
	private int pow(int value, int multiplier) {
		int power = 1;
		for(int i = 0; i < multiplier; i++) {
			power = power * value;
		}
		return power;
	}
}
