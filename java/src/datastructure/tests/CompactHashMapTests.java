package datastructure.tests;

import java.util.Arrays;
import java.util.Random;

import datastructure.CompactHashMap;
import unittest.UnitTest;

public class CompactHashMapTests extends UnitTest {
	public static void main(String[] args) {
		UnitTest u1 = new CompactHashMapTests();
	}
	
	public void testPutGet() {
		final int ENTRY_COUNT = 1000;
		CompactHashMap hashMap = new CompactHashMap(200000, 8);
		Random ran = new Random();
		byte[] value = new byte[8];
		for(int i = 0; i < ENTRY_COUNT; i++) {
			int key = ran.nextInt();
			ran.nextBytes(value);
			hashMap.put(key, value);
			byte[] retrievedValue = hashMap.get(key);
			assertTrue(Arrays.equals(retrievedValue, value), "Count: "+i+" Key: "+key+" Value: "+Arrays.toString(value)+" Retrieved: "+Arrays.toString(retrievedValue));
		}
		hashMap.printStats();
	}
}
