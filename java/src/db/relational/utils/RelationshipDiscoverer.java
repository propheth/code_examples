/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db.relational.utils;

import db.relational.Table;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RelationshipDiscoverer {
	private List<Table> learnTables(Connection conn) {
		List<Table> tables = new ArrayList<Table>();
		try {
			ResultSet rs1 = conn.getMetaData().getTables(null, null, null, null);
			while(rs1.next()) {
				String tableName = rs1.getString("TABLE_NAME");
				Table table = new Table(tableName);
				
				ResultSet rs2 = conn.getMetaData().getPrimaryKeys(null, null, tableName);
				while(rs2.next()) {
					String primaryKey = rs2.getString("COLUMN_NAME");
					table.addPrimaryKey(primaryKey);
				}
				
				ResultSet rs3 = conn.getMetaData().getColumns(null, null, tableName, null);
				while(rs3.next()) {
					String column = rs3.getString("COLUMN_NAME");
					table.addColumn(column);
				}
				
				tables.add(table);
			}
		} catch (SQLException ex) {
			
		}
		return tables;
	}
	
	public List<Table> discoverRelations(Connection conn, String foreignKeyColFormat) {
		List<Table> tables = learnTables(conn);
		for(Iterator<Table> it1 = tables.iterator(); it1.hasNext();) {
			Table thisTable = it1.next();
			String foreignKey = foreignKeyColFormat.replace("<TABLE_NAME>", thisTable.getTableName());
			thisTable.setForeignKey(foreignKey);
			for(Iterator<Table> it2 = tables.iterator(); it2.hasNext();) {
				Table checkTable = it2.next();
				if(thisTable.getTableName().equals(checkTable.getTableName())) {
					continue;
				}
				
				for(Iterator<String> it3 = checkTable.getColumns().iterator(); it3.hasNext();) {
					String column = it3.next();
					if(foreignKey.equals(column)) {
						thisTable.addDependant(checkTable);
						checkTable.addForeignKey(column, thisTable);
					}
				}
			}
		}
		return tables;
	}
}
