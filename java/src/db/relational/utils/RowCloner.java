/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db.relational.utils;

import db.relational.Table;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.sql.DataSource;

public class RowCloner {
	
	private static final String DEFAULT_LOG_FILE = "relrowcloner%g.log";
	
	private Logger log;
	private Map<Table, String> problemTables;
	
	class PrimaryKeyMap {
		private Map<String, String> oriToDup;
		
		public PrimaryKeyMap(Map<String, String> oriToDup) {
			this.oriToDup = oriToDup;
		}
		
		public Map<String, String> getOriToDup() {
			return oriToDup;
		}
	}
	
	public RowCloner() {
		defaultLog();
		problemTables = new LinkedHashMap<Table, String>();
	}
	
	/*
	* Clones application entity data model
	*/
	public PrimaryKeyMap cloneCascade(DataSource dataSource, Table parentTable, Table table, String columnName, String columnValue) {
		// initialize
		Map<String, String> oriToDupPkMap = new LinkedHashMap<String, String>();
		PrimaryKeyMap pkMaps = new PrimaryKeyMap(oriToDupPkMap);
		Connection conn = null;
		try {
			log.fine("\n=== "+table.getTableName()+" ===");
			conn = dataSource.getConnection();
			// get foreign keys of current table
			Set<String> foreignKeys = table.getForeignKeys().keySet();
			// get first primary key
			String primaryKey = table.getPrimaryKeys().size() > 0 ? table.getPrimaryKeys().get(0) : null;
			
			// base case
			// if there is only 1 foreign key and fk equals primary key of the row
			if(foreignKeys.size() == 1 && foreignKeys.iterator().next().equals(columnName)) {
				boolean dupsFound = false;
				boolean properDupsFound = false;
				boolean skipDup = false;
				
				String countToDup = "select count(*) from "+table.getTableName()+" where "+columnName+" = "+columnValue;
				Statement stmt1 = conn.createStatement();
				ResultSet rs1 = stmt1.executeQuery(countToDup);
				if(rs1.next()) {
					// get number of rows to clone
					int oriCount = rs1.getInt(1);
					stmt1.close();
					if(oriCount == 0) {
						// no original rows found
						skipDup = true;
					}
				}
				
				if(skipDup) {
					// skip no rows to clone
					log.fine("-- No rows exists for duplication");
				}
				else if(!dupsFound) {
					// none of the rows have been cloned yet
					String colsStr = columnsNoPrimaryKeys(table);
					if(primaryKey != null) {
						// current table has a primary key
						String selectOri = "select "+primaryKey+" from "+table.getTableName()+" where prosite_id = "+columnValue+" order by "+primaryKey+" asc";
						stmt1 = conn.createStatement();
						rs1 = stmt1.executeQuery(selectOri);
						while(rs1.next()) {
							// get primary key value for row to be cloned
							String oriPrimaryKeyVal = rs1.getString(primaryKey);
							// select insert of the row to be cloned based on the pk value
							String insertDup = "insert into "+table.getTableName()+" ("+colsStr+") select "+colsStr+" from "+table.getTableName()+" where "+primaryKey+" = "+oriPrimaryKeyVal;
							Statement stmt2 = conn.createStatement();
							// clone row
							stmt2.executeUpdate(insertDup, Statement.RETURN_GENERATED_KEYS);
							ResultSet rs2 = stmt2.getGeneratedKeys();
							if(rs2.next()) {
								// get the primary key value of the new cloned row
								String dupPrimaryKeyVal = rs2.getString(1);
								// link row to clone primary key value
								oriToDupPkMap.put(oriPrimaryKeyVal, dupPrimaryKeyVal);
							}
						}
					}
					else {
						// current table has no primary key
						// just clone rows 
						String insertDup = "insert into "+table.getTableName()+" ("+colsStr+") select "+colsStr+" from "+table.getTableName()+" where prosite_id = "+columnValue;
						Statement stmt2 = conn.createStatement();
						stmt2.executeUpdate(insertDup);
					}
				}
				
			}
			// recursive case
			else if(foreignKeys.size() > 0) {
				// go through each foreign key
				Map<String, PrimaryKeyMap> fkPkMaps = new HashMap<String, PrimaryKeyMap>();
				for(Iterator<String> it1 = foreignKeys.iterator(); it1.hasNext();) {
					String foreignKey = it1.next();
					Table foreignKeyTable = table.getForeignKeys().get(foreignKey);
					PrimaryKeyMap fkPkMap = null;
						
					// foreign key points back to current table
					if(parentTable != null 
							&& parentTable.getTableName().equals(table.getTableName())
							&& table.getTableName().equals(foreignKeyTable.getTableName())) {
						Map<String, String> oriToDup = new LinkedHashMap<String, String>();
						fkPkMap = new PrimaryKeyMap(oriToDup);
						// prevent infinite loop
						oriToDup.put("0", "0");
					}
					else {
						// recurse and get the original to cloned links for the current fk
						fkPkMap = cloneCascade(dataSource, table, foreignKeyTable, columnName, columnValue);
					}
					
					/* 
					* Check row-clone links on foreign key tables
					*/
					
					// unexpected problem down the chain
					if(fkPkMap == null) {
						log.fine("-- Problem down the chain. Stopping to prevent damage.");
						StringBuffer msg = new StringBuffer(problemTables.containsKey(table) ? problemTables.get(table) : "");
						msg.append("\n-- Problem down the chain. Stopping to prevent damage.");
						problemTables.put(table, msg.toString());
						// don't go any further and cause problems
						return null;
					}
					
					// no rows at foreign key table
					if(fkPkMap.getOriToDup().isEmpty()) {
						//log.fine("-- No rows from "+foreignKeyTable.getTableName());
					}
					// row-clone links found
					else {
						//log.fine("-- Got "+fkPkMap.getOriToDup().size()+" row(s) from "+foreignKeyTable.getTableName());
					}
					
					// store row-clone links for further processing later
					fkPkMaps.put(foreignKey, fkPkMap);
				}
				
				/* 
				* Build queries for rows and clones with relation to their
				* discovered foreign keys to check if there are any rows that
				* need to be cloned.
				* To check if any partially cloned rows exists or rows have
				* already been cloned.
				*  select from table where foreignkey1 in (1, 2, 3) and
				*   foreignkey2 in (a, b, c)
				* 
				*/
				
				boolean hasDepRows = false;
				// generate select query to check and duplicate rows in this table
				StringBuffer whereOriEntries = new StringBuffer();
				StringBuffer whereDupEntries = new StringBuffer();
				// original rows in values for all foreign keys
				for(Iterator<Map.Entry<String, PrimaryKeyMap>> it2 = fkPkMaps.entrySet().iterator(); it2.hasNext();) {
					Map.Entry<String, PrimaryKeyMap> fkPkMapsEntry = it2.next();
					StringBuffer inOriPkStr = new StringBuffer();
					StringBuffer inDupPkStr = new StringBuffer();
					// original rows in values
					for(Iterator<Map.Entry<String, String>> it3 = fkPkMapsEntry.getValue().getOriToDup().entrySet().iterator(); it3.hasNext();) {
						// row has dependencies that need to be addressed
						hasDepRows = true;
						if(inOriPkStr.length() > 0) {
							inOriPkStr.append(", ");
							inDupPkStr.append(", ");
						}
						Map.Entry<String, String> entry3 = it3.next();
						inOriPkStr.append(entry3.getKey());
						inDupPkStr.append(entry3.getValue());
					}
					
					if(inOriPkStr.length() > 0) {
						if(whereOriEntries.length() > 0) {
							whereOriEntries.append(" and ");
							whereDupEntries.append(" and ");
						}
						whereOriEntries.append(fkPkMapsEntry.getKey()).append(" in (");
						whereOriEntries.append(inOriPkStr).append(")");
						whereDupEntries.append(fkPkMapsEntry.getKey()).append(" in (");
						whereDupEntries.append(inDupPkStr).append(")");
					}
				}
				
				StringBuffer selectOriEntries = new StringBuffer("select * from ").append(table.getTableName()).append(" where ");
				selectOriEntries.append(whereOriEntries);
				if(primaryKey != null) {
					selectOriEntries.append(" order by ").append(primaryKey).append(" asc");
				}
				StringBuffer selectDupEntries = new StringBuffer("select * from ").append(table.getTableName()).append(" where ");
				selectDupEntries.append(whereDupEntries);
				if(primaryKey != null) {
					selectDupEntries.append(" order by ").append(primaryKey).append(" asc");
				}
				StringBuffer selectOriCountEntries = new StringBuffer("select count(*) from ").append(table.getTableName()).append(" where ");
				selectOriCountEntries.append(whereOriEntries);
				StringBuffer selectDupCountEntries = new StringBuffer("select count(*) from ").append(table.getTableName()).append(" where ");
				selectDupCountEntries.append(whereDupEntries);
				StringBuffer deleteBadDupEntries = new StringBuffer("delete from ").append(table.getTableName()).append(" where ").append(whereDupEntries);
				
				if(hasDepRows) {
					// found dependent rows at foreign key tables
					Statement stmt1 = conn.createStatement();
					ResultSet rs1 = stmt1.executeQuery(selectOriCountEntries.toString());
					log.finer(selectOriCountEntries.toString());
					int oriCount = 0;
					if(rs1.next()) {
						// count rows that need to be cloned in current table
						oriCount = rs1.getInt(1);
					}
					stmt1.close();
					
					stmt1 = conn.createStatement();
					rs1 = stmt1.executeQuery(selectDupCountEntries.toString());
					log.finer(selectDupCountEntries.toString());
					int dupCount = 0;
					if(rs1.next()) {
						// get any existing clones in current table
						dupCount = rs1.getInt(1);
					}
					stmt1.close();
					
					if(oriCount > 0 && dupCount == 0) {
						// found rows for cloning and table has no clones
						log.fine(selectOriEntries.toString());
						stmt1 = conn.createStatement();
						rs1 = stmt1.executeQuery(selectOriEntries.toString());
						// build comma separated list of cols without the pk col
						// colname2, colname3, colname4
						String colsStr = columnsNoPrimaryKeys(table);
						int addedDups = 0;
						while(rs1.next()) {
							// sql insert value list
							// colname2, colname3, colname4
							String valsStr = new String(colsStr);
							StringBuffer noPkWhere = new StringBuffer();
							for(Iterator<Map.Entry<String, PrimaryKeyMap>> it1 = fkPkMaps.entrySet().iterator(); it1.hasNext();) {
								Map.Entry<String, PrimaryKeyMap> entry1 = it1.next();
								String foreignKey = entry1.getKey();
								String oriForeignKeyVal = rs1.getString(foreignKey);
								String dupForeignKeyVal = entry1.getValue().getOriToDup().get(oriForeignKeyVal);
								if(dupForeignKeyVal != null) {
									// replace any foreign key in the value list
									// with its value
									// colname2, 4329, 'adam'
									try {
										Integer.parseInt(dupForeignKeyVal);
										// numeric foreign key value replacement
										valsStr = valsStr.replaceFirst("`"+foreignKey+"`", dupForeignKeyVal);
									}
									catch(NumberFormatException e) {
										// alphanumeric foreign key value replacement
										valsStr = valsStr.replaceFirst("`"+foreignKey+"`", "'"+dupForeignKeyVal+"'");
									}
									
									// make where
									// colname3 = 4329 and colname4 = 'adam'
									if(noPkWhere.length() > 0) {
										noPkWhere.append(" and ");
									}
									noPkWhere.append(foreignKey).append(" = ").append(oriForeignKeyVal);
								}
							}
							
							if(primaryKey != null) {
								String oriPrimaryKeyVal = rs1.getString(primaryKey);
								String insertDup = "insert into "+table.getTableName()+" ("+colsStr+") select "+valsStr+" from "+table.getTableName()+" where "+primaryKey+" = "+oriPrimaryKeyVal;
								log.fine(insertDup);
								Statement stmt2 = conn.createStatement();
								addedDups += stmt2.executeUpdate(insertDup, Statement.RETURN_GENERATED_KEYS);

								ResultSet rs2 = stmt2.getGeneratedKeys();
								if(rs2.next()) {
									String dupPrimaryKeyVal = rs2.getString(1);
									oriToDupPkMap.put(oriPrimaryKeyVal, dupPrimaryKeyVal);
								}
								stmt2.close();
								
							}
							else if(primaryKey == null) {
								String insertDup = "insert into "+table.getTableName()+" ("+colsStr+") select "+valsStr+" from "+table.getTableName()+" where "+noPkWhere.toString();
								Statement stmt2 = conn.createStatement();
								addedDups += stmt2.executeUpdate(insertDup);
								stmt2.close();
							}
							
							if(addedDups >= oriCount) {
								break;
							}
						}
						stmt1.close();
					}
					else if(dupCount != oriCount) {
						// incomplete cloned rows
						// solution delete query
						log.fine("-- Bad set of duplicates found");
						log.fine("-- Solution: "+deleteBadDupEntries.toString());
						StringBuffer msg = new StringBuffer(problemTables.containsKey(table) ? problemTables.get(table) : "");
						msg.append("\n-- "+deleteBadDupEntries.toString());
						problemTables.put(table, msg.toString());
						pkMaps = null;
					}
					else if(oriCount > 0 && dupCount == oriCount) {
						// already cloned
						// primary key exist for current table
						if(primaryKey != null) {
							stmt1 = conn.createStatement();
							rs1 = stmt1.executeQuery(selectOriEntries.toString());
							Statement stmt2 = conn.createStatement();
							ResultSet rs2 = stmt2.executeQuery(selectDupEntries.toString());

							// should have the same number of rows
							while(rs1.next() && rs2.next()) {
								String oriPrimaryKeyVal = rs1.getString(primaryKey);
								String dupPrimaryKeyVal = rs2.getString(primaryKey);
								// link row-clone
								oriToDupPkMap.put(oriPrimaryKeyVal, dupPrimaryKeyVal);
							}

							stmt1.close();
							stmt2.close();
						}
					}
					else if(oriCount == 0) {
						// no rows found to be cloned
						log.fine("-- No rows found for duplication");
					}
				}
				else {
					// no foreign key dependencies found
					log.fine("-- No dependant rows found");
				}
				
			}
		} catch (Exception ex) {
			pkMaps = null;
			log.fine(Arrays.toString(ex.getStackTrace()));
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException ex) {
					log.fine(Arrays.toString(ex.getStackTrace()));
				}
			}
		}
		
		return pkMaps;
	}
	
	private String columnsNoPrimaryKeys(Table table) {
		StringBuffer columnsStr = new StringBuffer();
		List<String> columns = table.getColumns();
		columns.removeAll(table.getPrimaryKeys());
		for(int i = 0; i < columns.size(); i++) {
			if(i > 0) {
				columnsStr.append(", ");
			}
			String column = "`"+columns.get(i)+"`";
			columnsStr.append(column);
		}
		return columnsStr.toString();
	}
	
	public void setLog(Logger log) {
		this.log = log;
	}
	
	public void defaultLog() {
		log = Logger.getLogger(this.getClass().getName());
		log.setLevel(Level.ALL);
		try {
			Handler fh = new FileHandler(DEFAULT_LOG_FILE, 10000000, 1, true);
			fh.setFormatter(new java.util.logging.Formatter() {
				public String format(LogRecord record) {
					String formatted = record.getMessage();
					if(record.getLevel() == Level.INFO) {
						formatted = (new Date(record.getMillis())).toString() + "# " + formatted;
					}
					return formatted + "\r\n";
				}
			});
			log.addHandler(fh);
		}
		catch(IOException e) {}
	}
}
