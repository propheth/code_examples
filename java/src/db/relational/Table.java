/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db.relational;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Table {
		private String tableName;
		private List<String> primaryKeys;
		private List<String> columns;
		private List<Table> dependantOnTables;
		private Map<String, Table> foreignKeys;
		private String foreignKey;
		
		public Table(String tableName) {
			this.tableName = tableName;
			primaryKeys = new ArrayList<String>();
			columns = new ArrayList<String>();
			dependantOnTables = new ArrayList<Table>();
			foreignKeys = new LinkedHashMap<String, Table>();
		}
		
		public void addPrimaryKey(String primaryKey) {
			primaryKeys.add(primaryKey);
		}
		
		public List<String> getPrimaryKeys() {
			return primaryKeys;
		}
		
		public void addForeignKey(String foreignKey, Table table) {
			foreignKeys.put(foreignKey, table);
		}
		
		public Map<String, Table> getForeignKeys() {
			return foreignKeys;
		}
		
		public void addColumn(String column) {
			columns.add(column);
		}
		
		public void addDependant(Table table) {
			dependantOnTables.add(table);
		}
		
		public void setForeignKey(String foreignKey) {
			this.foreignKey = foreignKey;
		}
		
		public String getForeignKey() {
			return foreignKey;
		}
		
		public List<Table> getDependants() {
			return dependantOnTables;
		}
		
		public List<String> getColumns() {
			return columns;
		}
		
		public String getTableName() {
			return tableName;
		}
		
		public String toString() {
			StringBuffer str = new StringBuffer();
			str.append(tableName);
			for(Iterator<String> it = primaryKeys.iterator(); it.hasNext();) {
				String primaryKey = it.next();
				str.append(" [").append(primaryKey).append("]");
			}
			
			str.append("{");
			StringBuffer fksStr = new StringBuffer();
			for(Iterator<Map.Entry<String, Table>> it = foreignKeys.entrySet().iterator(); it.hasNext();) {
				if(fksStr.length() > 0) {
					fksStr.append(", ");
				}
				Map.Entry<String, Table> entry = it.next();
				String foreignKey = entry.getKey();
				Table foreignTable = entry.getValue();
				fksStr.append(foreignKey).append(" -> ").append(foreignTable.getTableName());
			}
			str.append(fksStr);
			str.append("}");
			return str.toString();
		}
	}
