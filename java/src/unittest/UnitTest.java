package unittest;

/*
* Unit testing framework
*/

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UnitTest {
	
	protected final int MIN_PADDING = 8;
	
	protected Map<String, Boolean> _setup;
	protected Map<String, Boolean> _tests;
	protected Map<String, Integer> _errLineNumbers;
	protected Map<String, String> _errMsg;
	
	public UnitTest() {
		_setup = new LinkedHashMap<String, Boolean>();
		_tests = new LinkedHashMap<String, Boolean>();
		_errLineNumbers = new HashMap<String, Integer>();
		_errMsg = new HashMap<String, String>();
		
		String testClassPath = getTestClassPath();
		List<String> testMethods = getAllTestAndSetupMethods();
		
		for(Iterator it = testMethods.iterator(); it.hasNext();) {
			String methodName = (String)it.next();
			if(methodName.startsWith("setup")) {
				callMethod(this, methodName, new Object[0]);
				_setup.put(methodName, null);
				it.remove();
			}
		}
		
		for(Iterator it = testMethods.iterator(); it.hasNext();) {
			String testMethod = (String)it.next();
			String testName = testClassPath+"::"+testMethod;
			_tests.put(testName, null);
			callMethod(this, testMethod, new Object[0]);
		}
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				String results = "";
				List<String> testLines = new ArrayList<String>();
				List<Boolean> testResults = new ArrayList<Boolean>();
				for(Iterator it = _tests.keySet().iterator(); it.hasNext();) {
					String testName = (String)it.next();
					String[] testClassFunc = testName.split("::");				
					Boolean testResult = _tests.get(testName);
					testLines.add(testClassFunc[1]);
					testResults.add(testResult);
				}
				
				List<String> paddedLines = padLines(testLines, MIN_PADDING);
				String testClassPath = getTestClassPath();
				results = "==="+testClassPath+"===\r\n";
				if(_setup.size() > 0) {
					results += "  -> ";
					for(Iterator it = _setup.keySet().iterator(); it.hasNext();) {
						String setupName = (String)it.next();
						results += setupName+", ";
					}
					results = results.substring(0, results.length()-2);
					results += "\n";
				}
				for(int i = 0; i < paddedLines.size(); i++) {
					String paddedLine = paddedLines.get(i);
					Boolean testResult = testResults.get(i);
					String testName = testClassPath+"::"+testLines.get(i);
					if(testResult == null) {
						results += "  "+paddedLine+"   ?        \r\n";
					}
					else if(testResult) {
						results += "  "+paddedLine+"[PASSED]    \r\n";
					}
					else {
						Integer errLineNumber = _errLineNumbers.get(testName);
						String errMsg = _errMsg.get(testName);
						results += "  "+paddedLine+"FAILED @ Line "+errLineNumber;
						if(errMsg != null) {
							results += " "+errMsg;
						}
						results += "\n";
					}
				}
				System.out.println(results);
			}
		});
	}
	
	private List<String> getAllTestAndSetupMethods() {
		List<String> testMethods = new ArrayList<String>(); 
		Class c = this.getClass();
		Method[] methods = c.getDeclaredMethods();
		for(int i = 0; i < methods.length; i++) {
			Method method = methods[i];
			if(method.getName().startsWith("test") 
					|| method.getName().startsWith("setup")) {
				testMethods.add(method.getName());
			}
		}
		return testMethods;
	}
	
	public void assertTrue(boolean test) {
		String testName = getTestName();
		if(!_tests.containsKey(testName) ||
				_tests.get(testName) == null ||
				!_tests.get(testName).equals(Boolean.FALSE)) {
			if(!test) {
				_tests.put(testName, Boolean.FALSE);
				_errLineNumbers.put(testName, getTestLineNumber());
			}
			else {
				_tests.put(testName, Boolean.TRUE);
			}
		}
	}
	
	public void assertTrue(boolean test, String msg) {
		String testName = getTestName();
		if(!_tests.containsKey(testName) ||
				_tests.get(testName) == null ||
				!_tests.get(testName).equals(Boolean.FALSE)) {
			if(!test) {
				_tests.put(testName, Boolean.FALSE);
				_errLineNumbers.put(testName, getTestLineNumber());
				_errMsg.put(testName, msg);
			}
			else {
				_tests.put(testName, Boolean.TRUE);
			}
		}
	}
	
	public Object callMethod(Object obj, String methodName, Object[] params) {
		Object retVal = null;
		Class clazz =  obj instanceof Class ? (Class)obj : obj.getClass();
		Class[] paramsType = new Class[params.length];
		for(int i = 0; i < params.length; i++) {
			paramsType[i] = params[i].getClass();
		}
		try {
			Method method = clazz.getDeclaredMethod(methodName, paramsType);
			method.setAccessible(true);
			try {
				retVal = method.invoke(obj, params);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
				System.exit(1);
			}
		} catch (NoSuchMethodException e) {
			boolean foundMethod = false;
			Method[] methods = clazz.getDeclaredMethods();
			for(int i = 0; i < methods.length; i++) {
				Method method = methods[i];
				if(method.getName().equals(methodName)) {
					method.setAccessible(true);
					try {
						retVal = method.invoke(obj, params);
						foundMethod = true;
						break;
					} catch (IllegalAccessException ex) {
						e.initCause(ex);
					} catch (IllegalArgumentException ex) {
						e.initCause(ex);
					} catch (InvocationTargetException ex) {
						e.initCause(ex);
					}
				}
			}
			
			if(!foundMethod) {
				e.printStackTrace();
			}
		} catch (SecurityException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return retVal;
	}
	
	private String getTestName() {
		Throwable t = new Throwable(); 
		StackTraceElement[] elements = t.getStackTrace(); 
		StackTraceElement element = elements[2];
		String testClassPath = element.getClassName();
		String testMethodName = element.getMethodName();
		return testClassPath+"::"+testMethodName;
	}
	
	private String getTestClassPath() {
		Class clazz = this.getClass();
		return clazz.getCanonicalName();
	}
	
	private int getTestLineNumber() {
		Throwable t = new Throwable(); 
		StackTraceElement[] elements = t.getStackTrace(); 
		StackTraceElement element = elements[2];
		return element.getLineNumber();
	}
	
	protected String getSpaces(int thisLine, int longestLine, int padding) {
		StringBuffer spaces = new StringBuffer();
		int count = longestLine - thisLine + padding;
		for(int i = 0; i < count; i++) {
			spaces.append(" ");
		}
		return spaces.toString();
	}
	
	private List<String> padLines(Collection<String> lines, int padding) {
		List<String> paddedLines = new ArrayList<String>();
		int longestLen = 0;
		for(Iterator it = lines.iterator(); it.hasNext();) {
			String line = (String)it.next();
			longestLen = line.length() > longestLen ? line.length() : longestLen;
		}
		for(Iterator it = lines.iterator(); it.hasNext();) {
			String line = (String)it.next();
			line += getSpaces(line.length(), longestLen, padding);
			paddedLines.add(line);
		}
		return paddedLines;
	}
}
