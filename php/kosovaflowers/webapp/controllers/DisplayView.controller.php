<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/Controller.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	
	class DisplayView extends Controller {
		protected function process(&$httpRequest, &$httpResponse) {
			$server = $httpRequest->server;
			$viewName = $server['REQUEST_URI'];
			$viewName = str_replace(ROOT_REL_TO_DOC_ROOT."/", "", $viewName);
			return new View($viewName);
		}
	}
?>