<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewRedirectRel.class.php");
	require_once(MODEL_PATH."/Paypal.model.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Cart.model.php");
	require_once(MODEL_PATH."/Order.model.php");
	require_once(MODEL_PATH."/Delivery.model.php");
	require_once(MODEL_PATH."/Email.model.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	
	/*
		Waits for IPN from paypal once payment is complete.
	*/
	
	class PaypalCheckoutReturn extends OldController {
		// on receive notification from paypal
		public function process(&$request, &$session, $server) {
			global $paypalAuth;
			global $dbConf;
			// check for token
			if(isset($request['token'])) {
				
				$db = Database::getInstance($dbConf);
				$languageCode = Model_Localization::getLanguageCode($session);
				$currencyCode = Model_Localization::getCurrencyCode($session);
				
				$token = $request['token'];
				$payerId = $request['PayerID'];
				$paypalUser = $paypalAuth['user'];
				$paypalPwd = $paypalAuth['pwd'];
				$paypalSignature = $paypalAuth['signature'];
				$paypalSandbox = $paypalAuth['sandbox'];

				// get paypal checkout customer info
				$response1 = Paypal::getExpressCheckoutDetails(
					$paypalUser,
					$paypalPwd,
					$paypalSignature,
					$paypalSandbox,
					urldecode($token)
				);
				
				$responseData1 = Http::getPostData($response1);
				
				if(isset($responseData1['AMT'])) {
					$amt = $responseData1['AMT'];
					$token = urlencode($responseData1['TOKEN']);
					$payerId = $responseData1['PAYERID'];
					$customerEmail = $responseData1['EMAIL'];
					// is the orderId in the orders table
					$invoiceNum = $responseData1['PAYMENTREQUEST_0_INVNUM'];
					
					// finalize sale
					$response2 = Paypal::doExpressCheckoutPayment(
						$paypalUser,
						$paypalPwd,
						$paypalSignature,
						$paypalSandbox,
						$token,
						$payerId,
						$amt,
						$currencyCode
					);
					
					$responseData2 = Http::getPostData($response2);
					
					$view = null;
					// if sale successful then send email and show success view
					if(isset($responseData2['ACK']) && $responseData2['ACK'] == 'Success') {
						if($responseData2['PAYMENTINFO_0_ACK'] == 'Success') {
							
							Model_Order::pay($db, $invoiceNum, Model_Payment::PAYPAL_EXPRESS, Model_Payment::STATUS_CAPTURED);
							Model_Order::setPaid($db, $invoiceNum);
							
							$view = new ViewRedirectRel("payment-success?orderNumber={$invoiceNum}");
						}
					}
					else {
						$view = new View('paymentfailed.php');
					}
					
					return $view;
				}
			}
		}
	}

?>