<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewCode.class.php");

	class PaypalCheckoutCancel extends OldController {
		public function process(&$request, &$session, $server) {
			$html = "hello, world.";
			return new ViewCode($html);
		}
	}

?>