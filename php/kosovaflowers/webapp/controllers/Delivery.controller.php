<?php
	require_once(__DIR__."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/Controller.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewForm.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewRedirectRel.class.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Http.lib.php");
	require_once(WEB_FRAMEWORK_PATH."/Ex.exception.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Cart.model.php");
	require_once(MODEL_PATH."/Payment.model.php");
	require_once(MODEL_PATH."/GoogleCheckout.model.php");
	require_once(MODEL_PATH."/Paypal.model.php");
	require_once(MODEL_PATH."/PaypalStandard.model.php");
	require_once(MODEL_PATH."/TwoCheckout.model.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/Validator.class.php");
	require_once(MODEL_PATH."/Order.model.php");
	require_once(MODEL_PATH."/Location.model.php");
	require_once(ROOT_PATH."/objschema.php");
	
	class Delivery extends Controller {
		protected function process($httpRequest, $httpResponse) {
			$post = $httpRequest->post;
			$session = &$httpRequest->session;
			$server = $httpRequest->server;
			
			global $dbConf;
			global $paths;
			$db = Database::getInstance($dbConf);
			$languageCode = Model_Localization::getLanguageCode($session);
			$currencyCode = Model_Localization::getCurrencyCode($session);
			$locations = Model_Location::getLocationsByLocality($db, Model_Location::LOCALITY_CITY, $currencyCode);
			$cartId = Model_Cart::getCartId($session);
			$cartItems = Model_Cart::getCartItems($db, $session, $languageCode, $currencyCode);
			
			$deliveryFee = $locations[0]->fee;
			if(isset($post['city'])) {
				foreach($locations as $l) {
					if($l->name == $post['city']) {
						$deliveryFee = $l->fee;
					}
				}
			}
			$post['deliveryFee'] = $deliveryFee;
			
			if(empty($cartItems)) {
				return new ViewRedirectRel("cart");
			}
			
			$errors = isset($post['error']) ? array(urldecode($post['error'])) : null;
			
			// default view
			$model = array($cartItems, $locations, $post);
			$view = new ViewForm("delivery.php", $model, $errors);
			
			if(isset($post['continue'])) {
				// run the Delivery validator on the request
				$errors = validate("Delivery", $post);
				
				// Demo mode
				$errors = array(
					"demo" => "This website is a demo of the PP framework."
				);
				
				if($errors) {
					/* 
					  if there are errors then pass it to the view form
					  in the delivery.php view check $errors, display errors. 
					*/
					$model = array($cartItems, $locations, $post);
					$view = new ViewForm("delivery.php", $model, $errors);
				}
				else {
					// if no validation errors
					if($orderNumber = Model_Order::incompleteOrderExists($session)) {
						$paymentType = Model_Payment::getPaymentType($db, $orderNumber);
						$paymentStatus = Model_Payment::getPaymentStatus($db, $orderNumber);
						
						$successful = false; 
						
						if($paymentType == Model_Payment::PAYPAL_EXPRESS) {
							$successful = $paymentStatus == Model_Payment::STATUS_CAPTURED ? true : false;
						}
						else if($paymentType == Model_Payment::TWOCHECKOUT) {
							$successful = $paymentStatus == Model_Payment::STATUS_AUTHORIZED || $paymentStatus == Model_Payment::STATUS_CAPTURED ? true : false;
						}
						
						if($successful) {
							$errors[] = "Payment has already been made for this order. <a href=\"{$paths['REL_ROOT_URL']}/payment-success?orderNumber={$orderNumber}\">Click here</a> to view your invoice.";
							$view = new ViewForm("delivery.php", $model, $errors);
							return $view;
						}
					}
					
					/* create customer relational object */
					$customer = new RelObj_customer();
					$customer->email = $post['customerEmail'];
					$customer->phone1 = $post['customerPhone'];
					$customer->ip_addr = $server['REMOTE_ADDR'];
					
					/* create delivery relational object */
					$delivery = new RelObj_delivery();
					$delivery->name = $post['fullName'];
					$delivery->address1 = $post['address1'];
					$delivery->address2 = $post['address2'];
					$delivery->zip = $post['zip'];
					$delivery->city = $post['city'];
					$delivery->state = $post['state'];
					$delivery->country = $post['country'];
					$delivery->phone1 = $post['phone'];
					$delivery->delivery_date = $post['deliveryDate2'];
					$delivery->additional_instructions = $post['additionalInstructions'];
					$delivery->message = $post['message'];
					
					/* then createNewOrder in db */
					$orderNumber = Model_Order::createNewOrder($db, $session, $customer, $delivery, $cartItems, $deliveryFee);
					
					/* paypal express or 2checkout */
					if($post['paymentType'] == 'paypalExpress') {
						global $paypalAuth;
						$paypalUser = $paypalAuth['user'];
						$paypalPwd = $paypalAuth['pwd'];
						$paypalSignature = $paypalAuth['signature'];
						$paypalSandbox = $paypalAuth['sandbox'];
						$paypalReturnUrl = $paypalAuth['return_url'];
						$paypalCancelUrl = $paypalAuth['cancel_url'];
						
						try {
							$view = Paypal::setExpressCheckout(
								$paypalUser,
								$paypalPwd,
								$paypalSignature,
								$paypalSandbox,
								$paypalReturnUrl,
								$paypalCancelUrl,
								$currencyCode,
								$orderNumber,
								$cartItems,
								$deliveryFee
							);
						}
						catch(Ex $e) {
							// if paypal exception occurs show error
							//$errors[] = "[" . $e->getCode() . "] " . $e->getMessage();
							switch($e->getCode()) {
								case '10412':
									//$errors[] = "Payment has already been made for this order. <a href=\"{$paths['REL_ROOT_URL']}/payment-success?orderNumber={$orderNumber}\">Click here</a> to view your invoice.";
									$errors[] = $e->getMessage();
									break;
							}
							$view = new ViewForm("delivery.php", $model, $errors);
						}
						
					}
					else if($post['paymentType'] == 'twoCheckout') {
						global $twoCheckoutConf;
						$twoCheckoutSid = $twoCheckoutConf['sid'];
						$twoCheckoutDemo = $twoCheckoutConf['demo'];
						$twoCheckoutReceiptUrl = $twoCheckoutConf['receiptUrl'];
						$twoCheckoutReturnUrl = $twoCheckoutConf['returnUrl'];
						
						$view = TwoCheckout::checkout(
							$twoCheckoutSid,
							$twoCheckoutDemo,
							$twoCheckoutReceiptUrl,
							$twoCheckoutReturnUrl,
							TwoCheckout::MULTI_PAGE_CHECKOUT_TYPE,
							$orderNumber,
							$cartItems,
							$deliveryFee
						);
						
					}
				}
			}
			/* if order is incomplete */
			else if(($orderNumber = Model_Order::incompleteOrderExists($session)) && empty($httpRequest->post)) {
				$order = Model_Order::getOrder($db, $orderNumber);
				try {
					$customer = Model_Customer::getCustomerById($db, $order->customer_id);
				}
				catch(DatabaseEx $e) {
				
				}
				$delivery = Model_Delivery::getDelivery($db, $orderNumber);
				$post['customerEmail'] = $customer->email;
				$post['customerPhone'] = $customer->phone1;
				$post['fullName'] = $delivery->name;
				$post['address1'] = $delivery->address1;
				$post['address2'] = $delivery->address2;
				$post['zip'] = $delivery->zip;
				$post['city'] = $delivery->city;
				$post['state'] = $delivery->state;
				$post['country'] = $delivery->country;
				$post['phone'] = $delivery->phone1;
				$post['deliveryDate'] = $delivery->delivery_date;
				$post['deliveryDate2'] = $delivery->delivery_date;
				$post['additionalInstructions'] = $delivery->additional_instructions;
				$post['message'] = $delivery->message;
				
				$deliveryFee = $locations[0]->fee;
				if(isset($post['city'])) {
					foreach($locations as $l) {
						if($l->name == $post['city']) {
							$deliveryFee = $l->fee;
						}
					}
				}
				$post['deliveryFee'] = $deliveryFee;
				
				$errors1 = validate('Delivery', $post);
				if($errors) {
					$errors = array_merge($errors, $errors1);
				}
				else {
					$errors = $errors1;
				}
				
				$model = array($cartItems, $locations, $post);
				$view = new ViewForm("delivery.php", $model, $errors);
			}
			
			return $view;
		}
		
	}
	
?>