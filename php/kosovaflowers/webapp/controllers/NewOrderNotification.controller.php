<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Http.lib.php");
	
	class NewOrderNotification extends OldController {
		public function process(&$request, &$session, $server) {
			$o = print_r($request, true);
			$header = "HTTP/1.0 200 OK\r\nContent-Type: application/xml;charset=UTF-8\r\nAccept: application/xml;charset=UTF-8";
			//$header = "HTTP/1.0 404 Not Found";
			$serialNumber = "";
			if(isset($request['serial-number'])) {
				$serialNumber = $request['serial-number'];
			}
			$content = "_type=notification-acknowledgment&serial-number={$serialNumber}";
			$contentXml = "<notification-acknowledgment xmlns=\"http://checkout.google.com/schema/2\" serial-number=\"{$serialNumber}\" />";
			$response = Http::thisResponse($header, $contentXml);
			file_put_contents("new-order-notification.log", $o . "\n\n" . $response);
		}
	}
	
?>