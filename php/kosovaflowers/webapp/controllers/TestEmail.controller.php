<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewCode.class.php");
	require_once(MODEL_PATH."/Email.model.php");
	
	class TestEmail extends OldController {
		public function process(&$request, &$session, $server) {
			$to = 'navynmr@gmail.com';
			$subject = 'Test subject';
			$plainBody = 'Hello, world.';
			$htmlBody = '<b>Hello, world.</b>';
			$from = 'dev@telnet7.net';
			$err = Model_Email::sendMultipart($to, $subject, $plainBody, $htmlBody, $from);
			$testEmail = <<<EOS
			<i>{$from}</i><br/>
			<i>{$to}</i><br/>
			{$subject}<br/>
			<div>{$plainBody}</div>
			<div>{$htmlBody}</div>
			return status <code>{$err}</code>
EOS;
			return new ViewCode($testEmail);
		}
	}
	
?>