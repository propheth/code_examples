<?php
	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(WEB_FRAMEWORK_PATH."/UrlMapper.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/Controller.class.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Catalog.model.php");
	
	class CatalogDispatchers extends Controller {
		protected function process($httpRequest, $httpResponse) {
			global $dbConf;
			$db = Database::getInstance($dbConf);
			$seoNames = Model_Catalog::getSeoNames($db);
			
			foreach($seoNames as $seoName) {
				if(UrlMapper::makeControllerDispatcher("{$seoName['seo_name']}", "Catalog", true)) {
					print "{$seoName['seo_name']} => Catalog ... CREATED<br/>";
				}
			}
		}
	}
	
?>