<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/Controller.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Order.model.php");
	require_once(MODEL_PATH."/Delivery.model.php");
	
	class ViewOrderEmail extends Controller {
		
		protected function process($httpRequest, $httpResponse) {
			global $dbConf;
			$db = Database::getInstance($dbConf);
			$invoiceNum = $httpRequest->get['orderNumber'];
			$languageCode = Model_Localization::getLanguageCode($session);
			$currencyCode = Model_Localization::getCurrencyCode($session);
			$cartItems = Model_Order::getOrderItems($db, $invoiceNum, $languageCode, $currencyCode);
			$deliveryFee = Model_Order::getDeliveryFee($db, $invoiceNum);
			$customerEmail = Model_Order::getCustomerEmail($db, $invoiceNum);
			$delivery = Model_Delivery::getDelivery($db, $invoiceNum);
			$orderEmailModel = array($delivery, $cartItems, $deliveryFee);
			$orderEmailHtml = new View("orderemail.html.php", $orderEmailModel);
			return $orderEmailHtml;
		}
	
	}

?>