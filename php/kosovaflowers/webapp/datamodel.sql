﻿CREATE DATABASE kosovaflowers;
USE kosovaflowers;

CREATE TABLE catalog (
	id INT(8) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	seo_name VARCHAR(255) NOT NULL,
	catch_phrase VARCHAR(1000) NULL
) AUTO_INCREMENT = 100;

CREATE TABLE catalog_entry (
	id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	catalog_id INT(8) NOT NULL,
	item_id INT(12) NOT NULL,
	sort_by INT(12) NOT NULL DEFAULT 1000,
	date_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	status ENUM('SHOW', 'HIDE') NOT NULL DEFAULT 'HIDE'
) AUTO_INCREMENT = 100;

CREATE TABLE item (
	id INT(12) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	product_id VARCHAR(255) NOT NULL UNIQUE KEY,
	date_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	status ENUM('IN_STOCK', 'OUT_OF_STOCK', 'ACTIVE', 'NOT_ACTIVE') NOT NULL DEFAULT 'NOT_ACTIVE'
) AUTO_INCREMENT = 100;

CREATE TABLE item_attr (
	id INT(14) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	item_id INT(12) NOT NULL,
	language_code VARCHAR(16) NOT NULL,
	name VARCHAR(255) NOT NULL,
	value VARCHAR(65000) NOT NULL,
	status ENUM('ACTIVE', 'DELETED') NOT NULL DEFAULT 'ACTIVE'
) CHARACTER SET = utf8;

CREATE TABLE item_price (
	id INT(14) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	item_id INT(12) NOT NULL,
	currency_code VARCHAR(16) NOT NULL,
	price FLOAT NOT NULL DEFAULT 0.00
);

CREATE TABLE language (
	code VARCHAR(16) NOT NULL PRIMARY KEY,
	name VARCHAR(255) NOT NULL
);

CREATE TABLE currency (
	code VARCHAR(16) NOT NULL PRIMARY KEY,
	prefix VARCHAR(64) NOT NULL DEFAULT '',
	postfix VARCHAR(64) NOT NULL DEFAULT '',
	name VARCHAR(255) NOT NULL,
	entity VARCHAR(255) NOT NULL
);

CREATE TABLE cart (
	id INT(14) NOT NULL,
	item_id INT(12) NOT NULL,
	quantity INT(8) NOT NULL DEFAULT 1,
	last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	status ENUM('OPEN', 'EXPIRED', 'CONVERSION') NOT NULL DEFAULT 'OPEN'
);

CREATE TABLE payment_type (
	code VARCHAR(64) NOT NULL PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	available INT(1) NOT NULL DEFAULT 1
);

CREATE TABLE payment (
	id INT(12) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	order_number INT(12) NOT NULL,
	payment_type_code VARCHAR(32) NOT NULL,
	transaction_id VARCHAR(255) NOT NULL,
	status ENUM('PENDING', 'AUTHORIZED', 'CAPTURED', 'DECLINED', 'FRAUDULENT') NOT NULL DEFAULT 'PENDING',
	payment_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) AUTO_INCREMENT = 100;

CREATE TABLE customer (
	id INT(12) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(255) NOT NULL,
	phone1 VARCHAR(16) NOT NULL,
	ip_addr VARCHAR(128) NULL,
	city VARCHAR(255) NULL,
	region VARCHAR(255) NULL,
	country VARCHAR(255) NULL,
	country_code VARCHAR(8) NULL
) AUTO_INCREMENT = 100;

CREATE TABLE delivery (
	id INT(12) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	order_number INT(12) NOT NULL,
	name VARCHAR(255) NOT NULL,
	address1 VARCHAR(255) NOT NULL,
	address2 VARCHAR(255) NOT NULL,
	city VARCHAR(255) NOT NULL,
	state VARCHAR(255) NOT NULL,
	country VARCHAR(255) NOT NULL,
	zip VARCHAR(16) NOT NULL,
	phone1 VARCHAR(16) NOT NULL,
	phone2 VARCHAR(16) NULL,
	additional_instructions VARCHAR(1000) NOT NULL,
	message VARCHAR(255) NOT NULL,
	delivery_date DATE NOT NULL
) AUTO_INCREMENT = 100;

CREATE TABLE order_attr (
	order_number INT(12) NOT NULL,
	name VARCHAR(255) NOT NULL,
	value VARCHAR(65000) NOT NULL
) AUTO_INCREMENT = 100;

CREATE TABLE orders (
	number INT(12) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	customer_id INT(12) NOT NULL,
	paid INT(1) NOT NULL DEFAULT 0,
	status ENUM('PENDING', 'OUT_ON_DELIVERY', 'DELIVERED_TO_RECEIPIENT', 'RETURN_FOR_DELIVERY') NOT NULL DEFAULT 'PENDING',
	status_comment TEXT NOT NULL DEFAULT '',
	date_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) AUTO_INCREMENT = 100;

CREATE TABLE order_item (
	order_number INT(12) NOT NULL,
	product_id VARCHAR(255) NOT NULL,
	sort_by INT(12) NOT NULL DEFAULT 1000,
	quantity INT(8) NOT NULL,
	currency_code VARCHAR(16) NOT NULL,
	price FLOAT NOT NULL DEFAULT 0.00
);

CREATE TABLE location (
	name VARCHAR(255) NOT NULL,
	locality ENUM('ZIP', 'CITY', 'STATE', 'COUNTRY') NOT NULL DEFAULT 'CITY',
	active INT(1) NOT NULL DEFAULT 1
) CHARACTER SET = utf8;

CREATE TABLE location_fee (
	location_name VARCHAR(255) NOT NULL,
	currency_code VARCHAR(16) NOT NULL,
	fee FLOAT NOT NULL DEFAULT 0.00
) CHARACTER SET = utf8;

INSERT INTO payment_type (code, name, available) VALUES ('paypalexpress', 'paypal express checkout', 1);
INSERT INTO payment_type (code, name, available) VALUES ('paypalstandard', 'paypal standard checkout', 1);
INSERT INTO payment_type (code, name, available) VALUES ('googlecheckout', 'google checkout', 1);
INSERT INTO payment_type (code, name, available) VALUES ('2checkout', '2checkout', 1);

INSERT INTO language (code, name) VALUES ('en', 'english');
INSERT INTO language (code, name) VALUES ('sh', 'shqip');
INSERT INTO currency (code, prefix, name, entity) VALUES ('usd', '$', 'us dollar', 'United States');
INSERT INTO currency (code, prefix, name, entity) VALUES ('eur', 'EUR', 'euro', 'European Union');
