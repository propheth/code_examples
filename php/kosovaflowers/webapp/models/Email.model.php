<?php

	class Model_Email {
		public static function sendMultipart($to, $subject, $plainBody, $htmlBody, $from = null, $replyTo = null, $bcc = null) {
			$headers = null;
			$randomHash = md5(date('r', time()));
			
			if(isset($from)) {
				$headers .= "From: {$from}\n";
			}
			if(isset($replyTo)) {
				$headers .= "Reply-To: {$replyTo}\n";
			}
			if(isset($bcc)) {
				$headers .= "Bcc: {$bcc}\n";
			}
			
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-Type: multipart/alternative; boundary=\"{$randomHash}\"\n"; 
			
			$body = "";
			$body .= "\n--{$randomHash}\n";
			$body .= "Content-Type: text/plain; charset=\"UTF-8\"\n";
			$body .= "Content-Transfer-Encoding: 7bit\n";
			$body .= "\n";
			$body .= wordwrap(strip_tags($plainBody), 70);
			$body .= "\n--{$randomHash}\n";
			$body .= "Content-Type: text/html; charset=\"UTF-8\"\n";
			$body .= "Content-Transfer-Encoding: 7bit\n";
			$body .= "\n";
			$body .= $htmlBody;
			$body .= "\n--{$randomHash}--\n";
			
			$err = @mail($to, $subject, $body, $headers);
			
			return $err;
		}
	}
	
?>