<?php

	require_once(__DIR__."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/actions/Insert.class.php");
	
	class Model_Delivery {
		public static function addDelivery($db, $delivery) {
			$a = new Insert($db, $delivery);
			$r = $a->run();
			if($r) {
				return $db->getAutoIncrementValue();
			}
			return $r;
		}
		
		public static function getDelivery($db, $orderNumber) {
			$q1 = "SELECT * FROM delivery WHERE order_number = {$orderNumber}";
			$rs = $db->exec($q1);
			$delivery = null;
			if($rs->hasNext()) {
				$delivery = $rs->getNextObject();
			}
			return $delivery;
		}
		
		public static function deleteDelivery($db, $orderNumber) {
			$q1 = "DELETE FROM delivery WHERE order_number = {$orderNumber}";
			$rs = $db->exec($q1);
		}
	}

?>