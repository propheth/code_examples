<?php

	class Model_Lang {
		public static function setLanguageCode(&$session, $languageCode) {
			$session['language_code'] = $languageCode;
		}
		
		public static function getLanguageCode($session) {
			$languageCode = 'en';
			if(isset($session['language_code'])) {
				$languageCode = $session['language_code'];
			}
			return $languageCode;
		}
		
	}
?>