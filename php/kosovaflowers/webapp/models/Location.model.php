<?php

	require_once(__DIR__."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/Ex.exception.php");
	
	class Model_Location {
		const LOCALITY_ZIP = 'ZIP';
		const LOCALITY_CITY = 'CITY';
		const LOCALITY_STATE = 'STATE';
		const LOCALITY_COUNTRY = 'COUNTRY';
		
		public static function getLocationsByLocality($db, $locality, $currencyCode) {
			$locations = array();
			$q1 = "select * from location l, location_fee f where l.name = f.location_name and l.locality = '{$locality}' and f.currency_code = '{$currencyCode}'";
			$rs = $db->exec($q1);
			if($rs->hasNext()) {
				$locations = $rs->getAllObjects();
			}
			return $locations;
		}
		
		public static function getFee($db, $locationName, $locality, $currencyCode) {
			$fee = 0.00;
			$q1 = "select f.fee from location l, location_fee f where l.name = f.location_name and l.locality = '{$locality}' and f.currency_code = '{$currencyCode}' and l.name = '{$locationName}'";
			$rs = $db->exec($q1);
			if($rs->hasNext()) {
				$row = $rs->getNextRow();
				$fee = $row['fee'];
			}
			return $fee;
		}
	}

?>