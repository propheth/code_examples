<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Http.lib.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewRedirect.class.php");

	class TwoCheckout {
		const MULTI_PAGE_CHECKOUT_TYPE = 1;
		const SINGLE_PAGE_CHECKOUT_TYPE = 2;
		const DEFAULT_CHECKOUT_TYPE = self::MULTI_PAGE_CHECKOUT_TYPE;
		const TWOCHECKOUT_URL = "https://www.2checkout.com";
		const MULTI_PAGE_CHECKOUT_URL = 'https://www.2checkout.com/checkout/purchase';
		const SINGLE_PAGE_CHECKOUT_URL = 'https://www.2checkout.com/checkout/spurchase';
		// vendor account just incase 1436568
		// 1464822
		
		public static function checkout($sid, $demo, $receiptUrl, $returnUrl, $type, $orderId, $cartItems, $deliveryFee) {
			
			$formData = array(
				'sid' => $sid,
				'cart_order_id' => $orderId,
				'merchant_order_id' => $orderId,
				'id_type' => 1,
				'lang' => 'en',
				'skip_landing' => 1
			);
			
			if($demo) {
				$formData['demo'] = 'Y';
			}
			
			if($receiptUrl) {
				$formData['x_receipt_link_url'] = $receiptUrl;
			}
			
			if($returnUrl) {
				$formData['return_url'] = $returnUrl;
			}
			
			$total = 0.00;
			$i = 0;
			foreach($cartItems as $cartItem) {
				$i++;
				$formData["c_prod_{$i}"] = $cartItem['product_id'] . "," . $cartItem['quantity'];
				$formData["c_name_{$i}"] = $cartItem['name'];
				$formData["c_description_{$i}"] = $cartItem['name'];
				$formData["c_price_{$i}"] = $cartItem['price'] * $cartItem['quantity'];
				$total += $formData["c_price_{$i}"];
			}
			$i++;
			$formData["c_prod_{$i}"] = "Delivery fee";
			$formData["c_name_{$i}"] = "Delivery fee";
			$formData["c_description_{$i}"] = "Delivery fee";
			$formData["c_price_{$i}"] = $deliveryFee;
			$total += $deliveryFee;
			$formData["total"] = $total;
			
			$checkoutUrl = self::MULTI_PAGE_CHECKOUT_URL;
			if($type == self::MULTI_PAGE_CHECKOUT_TYPE) {
				$checkoutUrl = self::MULTI_PAGE_CHECKOUT_URL;
			}
			else if($type == self::SINGLE_PAGE_CHECKOUT_TYPE) {
				$checkoutUrl = self::SINGLE_PAGE_CHECKOUT_URL;
			}
			
			$checkoutUrl .= '?' . Http::buildQuery($formData);
			return new ViewRedirect($checkoutUrl);
		}
		
		public static function validateReceipt($request, $secretWord) {
			$sid = $request['sid'];
			$orderNumber = $request['order_number'];
			$total = $request['total'];
			$key = $request['key'];
			$a = $secretWord . $sid . $orderNumber . $total;
			$keyCheck = strtoupper(md5($a));
			return $key == $keyCheck;
		}
		
		public static function validateNotification($request, $secretWord) {
			$saleId = $request['sale_id'];
			$vendorId = $request['vendor_id'];
			$invoiceId = $request['invoice_id'];
			$hash = $request['md5_hash'];
			$a = $saleId . $vendorId . $invoiceId . $secretWord;
			$hashCheck = strtoupper(md5($a));
			return $hash == $hashCheck;
		}
		
		/*
			Secret Word => tango
			sid=> 123456
			order_number => 9999999
			total => 5.99
			md5hash = md5( tango12345699999995.99 )
			61A7621AC56A423ED204F401F767D75D
		*/
		public static function testValidateReceipt() {
			$secretWord = "tango";
			$request['sid'] = "123456";
			$request['order_number'] = "9999999";
			$request['total'] = "5.99";
			$request['key'] = "61A7621AC56A423ED204F401F767D75D";
			return self::validateReceipt($request, $secretWord);
		}
		
		/*
			sale_id => 9999999999
			vendor_id => 123456
			invoice_id => 1111111111
			Secret Word => tango
			md5hash = md5( 99999999991234561111111111tango )
			25B9A7DE486C2DB46031189D9C930564
		*/
		public static function testValidateNotification() {
			$secretWord = "tango";
			$request['sale_id'] = "9999999999";
			$request['vendor_id'] = "123456";
			$request['invoice_id'] = "1111111111";
			$request['md5_hash'] = "25B9A7DE486C2DB46031189D9C930564";
			return self::validateNotification($request, $secretWord);
		}
	}
	/*
	if(TwoCheckout::testValidateReceipt()) {
		print "testValidateReceipt ... PASSED";
	}
	else {
		print "testValidateReceipt ... FAILED";
	}
	*/
	/*
	if(TwoCheckout::testValidateNotification()) {
		print "testValidateNotification ... PASSED";
	}
	else {
		print "testValidateNotification ... FAILED";
	}
	*/
?>