<?php

	require_once(__DIR__."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/actions/Insert.class.php");
	require_once(WEB_FRAMEWORK_PATH."/Ex.exception.php");
	
	class Model_Payment {
		const PAYPAL_EXPRESS = 'paypalexpress';
		const PAYPAL_STANDARD = 'paypalstandard';
		const GOOGLE_CHECKOUT = 'googlecheckout';
		const TWOCHECKOUT = '2checkout';
		
		const STATUS_PENDING = 'PENDING';
		const STATUS_AUTHORIZED = 'AUTHORIZED';
		const STATUS_CAPTURED = 'CAPTURED';
		const STATUS_DECLINED = 'DECLINED';
		const STATUS_FRAUDULENT = 'FRAUDULENT';
		
		public static function addPayment($db, $payment) {
			$a = new Insert($db, $payment);
			$r = $a->run();
			if($r) {
				return $db->getAutoIncrementValue();
			}
			return $r;
		}
		
		public static function updatePaymentStatus($db, $orderNumber, $status) {
			// no need to sanitize inputs as they are provided internally
			$q1 = "update payment set status = '{$status}' where order_number = {$orderNumber}";
			$r = $db->exec($q1);
			if(!$r) {
				throw new Ex("error updating payment status");
			}
		}
		
		public static function getPaymentType($db, $orderNumber) {
			$paymentTypeCode = null;
			if(!empty($orderNumber)) {
				$q1 = "select p.payment_type_code from payment p, orders o where p.order_number = o.number and p.order_number = {$orderNumber}";
				$rs = $db->exec($q1);
				
				if($rs->hasNext()) {
					$row = $rs->getNextRow();
					$paymentTypeCode = $row['payment_type_code'];
				}
			}
			return $paymentTypeCode;
		}
		
		public static function getPaymentStatus($db, $orderNumber) {
			$paymentStatus = null;
			if(!empty($orderNumber)) {
				$q1 = "select p.status from payment p, orders o where p.order_number = o.number and p.order_number = {$orderNumber}";
				$rs = $db->exec($q1);
				
				if($rs->hasNext()) {
					$row = $rs->getNextRow();
					$paymentStatus = $row['status'];
				}
			}
			return $paymentStatus;
		}
	}

?>