<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	
	class Model_Catalog {
		public static function getCatalog($db, $catalogSeoName, $languageCode, $currencyCode) {
			$catalogSeoName = $db->escapeString($catalogSeoName);
			$rs = $db->exec("select ce.id as catalog_entry_id, i.id as item_id, i.product_id as product_id, c.name as catalog_name, ce.sort_by as catalog_entry_sort_by, group_concat(id.name separator '|') as item_attr_names, group_concat(id.value separator '|') as item_attr_values, ip.price as price, cur.prefix as currency_prefix from catalog c, catalog_entry ce, item i, item_attr id, item_price ip, currency cur where c.id = ce.catalog_id and ce.item_id = i.id and i.id = id.item_id and i.id = ip.item_id and ip.currency_code = cur.code and c.seo_name = '{$catalogSeoName}' and id.language_code = '{$languageCode}' and ip.currency_code = '{$currencyCode}' group by ce.id, i.id");
			$objects = array();
			if($rs->hasNext()) {
				$objects = $rs->getAllObjects();
			}
			$items = Model_Item::objectsToItems($objects);
			return $items;
		}
		
		public static function getCatalogName($db, $catalogSeoName) {
			$catalogSeoName = $db->escapeString($catalogSeoName);
			$rs = $db->exec("select name from catalog where seo_name = '{$catalogSeoName}'");
			$catalogName = "";
			if($rs->hasNext()) {
				$row = $rs->getNextRow();
				$catalogName = $row['name'];
			}
			return $catalogName;
		}
		
		public static function getSeoNames($db) {
			$catalogName = $db->escapeString($catalogName);
			$rs = $db->exec("select seo_name from catalog");
			$seoNames = array();
			if($rs->hasNext()) {
				$seoNames = $rs->getAllRows();
			}
			return $seoNames;
		}
		
		public static function getCatalogItems($db, $catalogSeoName, $languageCode, $currencyCode) {
			$catalogSeoName = $db->escapeString($catalogSeoName);
			$rs = $db->exec("select ce.id as catalog_entry_id, i.id as item_id, i.product_id as product_id, c.name as catalog_name, ce.sort_by as catalog_entry_sort_by, group_concat(id.name separator '|') as item_attr_names, group_concat(id.value separator '|') as item_attr_values, ip.price as price, cur.prefix as currency_prefix from catalog c, catalog_entry ce, item i, item_attr id, item_price ip, currency cur where c.id = ce.catalog_id and ce.item_id = i.id and i.id = id.item_id and i.id = ip.item_id and ip.currency_code = cur.code and c.seo_name = '{$catalogSeoName}' and id.language_code = '{$languageCode}' and ip.currency_code = '{$currencyCode}' group by ce.id, i.id");
			$objects = array();
			if($rs->hasNext()) {
				$objects = $rs->getAllObjects();
			}
			$items = Model_Item::objectsToItems($objects);
			return $items;
		}
	}

?>