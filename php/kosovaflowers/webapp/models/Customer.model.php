<?php

	require_once(__DIR__."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/actions/Insert.class.php");
	
	class Model_Customer {
		public static function addCustomer($db, $customer) {
			$customerId = null;
			$existingCustomer = self::getCustomer($db, $customer->email, $customer->phone1);
			if($existingCustomer) {
				$customerId = $existingCustomer->id;
			}
			else {
				$a = new Insert($db, $customer);
				$r = $a->run();
				if($r) {
					$customerId = $db->getAutoIncrementValue();
				}
			}
			return $customerId;
		}
		
		public static function getCustomer($db, $email, $phone) {
			$q1 = "SELECT * FROM customer WHERE email = '{$email}' AND phone1 = '{$phone}'";
			$rs = $db->exec($q1);
			$customer = null;
			if($rs->hasNext()) {
				$customer = $rs->getNextObject();
			}
			return $customer;
		}
		
		public static function getCustomerById($db, $customerId) {
			$q1 = "SELECT * FROM customer WHERE id = {$customerId}";
			$rs = $db->exec($q1);
			$customer = null;
			if($rs->hasNext()) {
				$customer = $rs->getNextObject();
			}
			return $customer;
		}
	}
	

?>