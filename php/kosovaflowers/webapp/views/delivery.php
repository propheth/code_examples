<?php
	list($cartItems, $locations, $formData) = $model;
	@extract($formData);
	$message = isset($message) ? $message : "Te Dua Zemer..";
	$actionUrl = formAction("delivery");
	$defaultDate = isset($deliveryDate) ? date_format(date_create($deliveryDate), "j F, Y") : date_format(date_create("+2 day 00:00:00"), "j F, Y");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Delivery info</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="<?php print PUBLIC_URL; ?>/css/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="<?php print PUBLIC_URL; ?>/css/default.css" rel="stylesheet" type="text/css" />
		<link href="<?php print PUBLIC_URL; ?>/jquery/themes/ui-lightness/jquery.ui.all.css" rel="stylesheet" type="text/css" />
		<script src="<?php print PUBLIC_URL; ?>/jquery/jquery-1.4.4.js"></script>
		<script src="<?php print PUBLIC_URL; ?>/jquery/ui/jquery.ui.core.js"></script>
		<script src="<?php print PUBLIC_URL; ?>/jquery/ui/jquery.ui.widget.js"></script>
		<script src="<?php print PUBLIC_URL; ?>/jquery/ui/jquery.ui.datepicker.js"></script>
		<script src="<?php print PUBLIC_URL; ?>/jquery/ui/jquery.effects.core.js"></script>
		<script src="<?php print PUBLIC_URL; ?>/jquery/ui/jquery.effects.drop.js"></script>
		<script type="text/javascript">
			$(function() {
				$("#deliverOn").datepicker({
					showOn: "button",
					buttonImage: "<?php print PUBLIC_URL; ?>/images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'd MM, yy',
					altField: "#deliverOn2",
					altFormat: 'yy-mm-dd',
					minDate: +1,
					defaultDate: +2,
					showAnim: 'drop',
					showOn: 'both',
					buttonText: 'pick a delivery date'
				});
				$("#deliverOn").datepicker("setDate", '<?php print $defaultDate; ?>');
				
				$('select[name=city]').change(function() {
					document.delivery.submit();
				});
			});
		</script>
		<style type="text/css">
			
			#content-box {
				position: relative;
				left: 0px;
				width: 100%;
				min-width: 900px;
				padding: 0;
				margin: 0;
			}
			
			#cart-box {
				float: left;
				width: 320px;
				margin-top: 10px;
				font-family: arial;
				font-size: 8pt;
			}
			
			.cart-table {
				width: 300px;
				border: 1px solid grey;
				margin: 10px 0;
			}
			
			.cart-table th {
				text-align: center;
				padding: 10px;
			}
			
			.cart-table td {
				padding: 10px;
				font-weight: bold;
			}
			
			.cart-column-header {
				border: 1px solid grey;
			}
			
			.cart-item {
				
			}
			
			.cart-item-number {
				border-left: 1px solid grey;
				border-right: 1px solid grey;
				text-align: center;
			}
			
			.cart-item-name {
			}
			
			.cart-item-quantity {
				border-left: 1px solid grey;
				border-right: 1px solid grey;
				text-align: center;
			}
			
			td.cart-item-price {
				border-left: 1px solid grey;
				border-right: 1px solid grey;
			}
			
			td.cart-total-label {
				padding-right: 12px;
				text-align: right;
				border: 1px solid grey;
			}
			
			td.cart-total-price {
				border: 1px solid grey;
				width: 65px;
			}
			
			#delivery-location {
				float: left;
				width: 400px;
				margin-left: 20px;
				margin-top: 10px;
				margin-bottom: 30px;
				/*border: 1px solid grey;*/
				font-family: arial;
			}
			
			.your-info-table > caption {
				font-size: 11pt;
				font-style: italic;
				padding: 10px;
			}
			
			.delivery-location-table > caption {
				font-size: 11pt;
				font-style: italic;
				padding: 10px;
			}
			
			.your-info-table td {
				padding: 3px 5px;
			}
			
			.delivery-location-table td {
				padding: 3px 5px;
			}
			
			.delivery-location-label {
				width: 100px;
				font-size: 10pt;
				text-align: right;
			}
			
			.delivery-location-text-input {
				width: 300px;
				font-family: times new roman;
				font-size: 12pt;
				font-weight: bold;
			}
			
			.delivery-location-text-input > input {
				padding: 2.5px;
			}
			
			.additional-instructions {
				vertical-align: top;
			}
			
			.additional-instructions-label {
				width: 100px;
				font-size: 10pt;
				text-align: right;
				padding: 10px 10px;
			}
			
			.additional-instructions-text-input {
				width: 300px;
				font-family: times new roman;
				font-size: 12pt;
				font-weight: bold;

			}
			
			.additional-instructions-text-input > textarea {
				padding: 3px;
			}
			
			.address-input > input {
				width: 230px;
			}
			
			table#payment-type > td {
				padding: 10px;
			}
			
			table#payment-type > caption {
				font-size: 11pt;
				font-style: italic;
				padding: 5px;
			}
			
			#deliverOn {
				margin-right: 5px;
				width: 150px;
			}
			
			div#message-card {
				padding: 0;
				margin-top: 20px;
				margin-left: 0px;
				width: 300px;
				height: 200px;
				background: url('<?php print PUBLIC_URL."/images/flowercard4.png"; ?>') no-repeat;
			}
			
			div#message-card > div {
				position: relative;
				top: 17px;
				left: 20px;
				font-size: 9pt;
				font-family: georgia;
			}
			
			div#message-card > div > textarea {
				opacity: 0.6;
				filter:alpha(opacity=60)
				margin-top: 10px;
				padding: 5px;
				border: 0;
				font-size: 12pt;
			}
			
			#continue-form {
				margin-top: 10px;
				text-align: left;
				background-color: #f4e9bd;
				padding: 5px 0;
				margin-left: 160px;
			}
			
			#continue-form > input[type=submit] {
				position: relative;
				left: 600px;
				font-size: 8pt;
				font-weight: bold;
				padding: 4px;
				font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
			}
			
			div.error {
				text-align: center;
				width: 280px;
				color: #db391e;
				background-color: #eaeaae;
				font-family: arial;
				font-size: 9pt;
				padding: 5px;
				margin: 0 auto;
				font-weight: bold;
			}
			
			.ui-widget {
				font-size: 0.7em;
			}
			
		</style>
	</head>
	<body>
	<div id="wrapper">
		
		<?php dispatch("Banner"); ?>
		<?php include_once(dirname(__FILE__)."/leftbar.php"); ?>
		
		<div id="content-box">
		
		<form name="delivery" action="<?php print $actionUrl; ?>" method="post">
			<input name="country" type="hidden" value="Kosovo" readonly="readonly" />
		<div id="continue-form"><input name="continue" type="submit" value="Next:Pay >>"/></div>
		
		<div id="delivery-location">
			
			<?php if($errors) { ?>
				<div class="error">
				<?php 
					foreach($errors as $field => $msg) {
						print $msg . "<br/>";
					} 
				?>
				</div>
			<?php } ?>
			
			<table class="your-info-table">
				<caption>Please enter your contact infomation</caption>
				<tr>
					<td class="delivery-location-label">Email</td>
					<td class="delivery-location-text-input"><input name="customerEmail" type="text" value="<?php print @$customerEmail; ?>" /></td>
				</tr>
				<tr>
					<td class="delivery-location-label">Phone</td>
					<td class="delivery-location-text-input"><input name="customerPhone" type="text" value="<?php @print $customerPhone; ?>" /></td>
				</tr>
			</table>
			
			<table class="delivery-location-table">
				<caption>Please enter the delivery information</caption>
				<tr>
					<td class="delivery-location-label">Recipient's Name</td>
					<td class="delivery-location-text-input">
						<input style="width: 200px;" name="fullName" type="text" value="<?php @print $fullName; ?>" />
					</td>
				</tr>
				<tr>
					<td class="delivery-location-label">Address 1</td>
					<td class="delivery-location-text-input address-input"><input name="address1" type="text" value="<?php @print $address1; ?>" /></td>
				</tr>
				<tr>
					<td class="delivery-location-label">Address 2</td>
					<td class="delivery-location-text-input address-input"><input name="address2" type="text" value="<?php @print $address2; ?>" /></td>
				</tr>
				<tr>
					<td class="delivery-location-label">Zip</td>
					<td class="delivery-location-text-input"><input style="width: 85px;" name="zip" type="text" value="<?php @print $zip; ?>" /></td>
				</tr>
				<tr>
					<td class="delivery-location-label">City</td>
					<td class="delivery-location-text-input">
						<select name="city" style="width: 150px;">
						<?php foreach($locations as $l) { ?>
							<option value="<?php @print $l->name; ?>" <?php if($l->name == @$city) { ?>selected="selected"<?php } ?>><?php @print $l->name; ?></option>
						<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="delivery-location-label">State</td>
					<td class="delivery-location-text-input"><input style="width: 120px;" name="state" type="text" value="Kosova" readonly="readonly" /></td>
				</tr>
				<!--
				<tr>
					<td class="delivery-location-label">Country</td>
					<td class="delivery-location-text-input"><input style="width: 150px;" name="country" type="text" value="Kosovo" readonly="readonly" /></td>
				</tr>
				-->
				<tr>
					<td class="delivery-location-label">Phone</td>
					<td class="delivery-location-text-input"><input style="width: 150px;" name="phone" type="text" value="<?php @print $phone; ?>" /></td>
				</tr>
				<tr>
					<td class="delivery-location-label">Deliver on</td>
					<td class="delivery-location-text-input">
						<input id="deliverOn" name="deliveryDate" type="text" value="" readonly="readonly" />
						<input id="deliverOn2" name="deliveryDate2" type="hidden" value="" />
					</td>
				</tr>
				<tr>
					<td class="additional-instructions-label">Additional Instructions</td>
					<td class="additional-instructions-text-input">
						<textarea name="additionalInstructions" rows="4" cols="30"><?php @print $additionalInstructions; ?></textarea>
					</td>
				</tr>
			</table>
		</div>
		
		<div id="cart-box">
			<table class="cart-table">
				<thead>
				<tr class="cart-column-header">
					<th></th>
					<th>Item</th>
					<th>Qty</th>
					<th>Price</th>
				</tr>
				</thead>
				<tbody>
				<?php
				global $paths;
				$o = "";
				$totalPrice = 0;
				if($cartItems)
				foreach($cartItems as $i => $cartItem) {
					$number = $i + 1;
					$quantityPrice = $cartItem['price'] * $cartItem['quantity'];
					$totalPrice += $quantityPrice;
					$quantityPrice = number_format($quantityPrice, 2, '.', '');
					$itemName = ucwords(strtolower($cartItem['name']));
					
					$o .= <<<EOS
					<tr class="cart-item">
						<td class="cart-item-number">{$number}</td>
						<td class="cart-item-name">{$itemName}<br/><span style="font-size: 6pt;">({$cartItem['product_id']})</span></td>
						<td class="cart-item-quantity">{$cartItem['quantity']}</td>
						<td class="cart-item-price">{$cartItem['currency_prefix']} {$quantityPrice}</td>
					</tr>
EOS;
				}
				$totalPrice += $deliveryFee;
				$deliveryFee = number_format($deliveryFee, 2, '.', '');
				$totalPrice = number_format($totalPrice, 2, '.', '');
				$o .= <<<EOS
				<tr class="cart-item">
					<td colspan="3" class="cart-total-label">Delivery</td>
					<td class="cart-total-price">{$cartItems[0]['currency_prefix']} {$deliveryFee}</td>
				</tr>
				<tr class="cart-item">
					<td colspan="3" class="cart-total-label">Total</td>
					<td class="cart-total-price">{$cartItems[0]['currency_prefix']} {$totalPrice}</td>
				</tr>
EOS;
				@print $o;
				
				?>
				</tbody>
			</table>
			<table id="payment-type">
				<caption>Select a payment method</caption>
				<tr>
				<td style="padding: 0 5px;"><table><tr><td style="vertical-align: middle;"><input type="radio" name="paymentType" value="paypalExpress" <?php @print ($paymentType == "paypalExpress") ? 'checked="checked"' : ''; ?> /></td><td><img style="margin: 5px;" src="<?php @print "{$paths['PUBLIC_URL']}/images/paypal.png"; ?>" /></td></tr></table></td>
				<td style="padding: 0 5px;"><table><tr><td style="vertical-align: middle;"><input type="radio" name="paymentType" value="twoCheckout" <?php @print ($paymentType == "twoCheckout") ? 'checked="checked"' : ''; ?> /></td><td><img style="margin: 5px;" src="<?php @print "{$paths['PUBLIC_URL']}/images/visa-mastercard.png"; ?>" /></td></tr></table></td>
				</tr>
			</table>
			<div id="message-card">
				<div>Enter your message here:<br/>
					<textarea name="message" rows="6" cols="29"><?php @print $message; ?></textarea>
				</div>
			</div>
		</div>
		</form>
		
		</div>
		<?php include_once(dirname(__FILE__)."/footer.php"); ?>
	</div>
	</body>
</html>