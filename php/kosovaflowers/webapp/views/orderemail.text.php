<?php
	list($delivery, $cartItems, $deliveryFee) = $model;
?>
Thank you for you order.

Your order number is <?php print $delivery->order_number; ?>


Delivery will be made to <?php print $delivery->name; ?> at <?php print "{$delivery->address1}, " . ($delivery->address2 ? "{$delivery->address2}, " : "") . "{$delivery->zip} {$delivery->city}, {$delivery->state}"; ?> on <?php print date_format(date_create($delivery->delivery_date), "l, jS F"); ?>


<?php if(strlen($delivery->additional_instructions) > 0) { ?>
Additional instructions, <?php print $delivery->additional_instructions; ?>


<?php } ?>
<?php
		$totalPrice = 0;
		if($cartItems)
		foreach($cartItems as $i => $cartItem) {
			$number = $i + 1;
			$quantityPrice = $cartItem['price'] * $cartItem['quantity'];
			$totalPrice += $quantityPrice;
			$quantityPrice = number_format($quantityPrice, 2, '.', '');
			$itemName = ucwords(strtolower($cartItem['name']));
?>
<?php print $number ?>	<?php print "{$itemName} ({$cartItem['product_id']})"; ?>	Qty: <?php print $cartItem['quantity']; ?>	Price: <?php print "{$cartItem['currency_prefix']} {$quantityPrice}"; ?>

<?php
		}
		$totalPrice += $deliveryFee;
		$deliveryFee = number_format($deliveryFee, 2, '.', '');
		$totalPrice = number_format($totalPrice, 2, '.', '');
?>

Delivery	<?php print "{$cartItems[0]['currency_prefix']} {$deliveryFee}"; ?>

Total		<?php print "{$cartItems[0]['currency_prefix']} {$totalPrice}"; ?>