<?php
	list($customerEmail, $cartItems, $delivery, $deliveryFee) = $model;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Delivery info</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="<?php print PUBLIC_URL; ?>/css/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="<?php print PUBLIC_URL; ?>/css/default.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			$(function() {
			
			});
		</script>
		<style type="text/css">
			
			#content-box {
				position: relative;
				left: 0px;
				width: 100%;
				min-width: 900px;
				padding: 0;
				margin: 0;
			}
			
			#cart-box {
				width: 650px;
				margin-top: 25px;
				font-family: arial;
				font-size: 8pt;
			}
			
			.cart-table {
				width: 600px;
				border: 1px solid grey;
				margin: 10px 0;
				font-size: 8pt;
			}
			
			.cart-table th {
				text-align: center;
				padding: 10px;
			}
			
			.cart-table td {
				padding: 10px;
				font-weight: bold;
			}
			
			.cart-column-header {
				border: 1px solid grey;
			}
			
			.cart-item {
				
			}
			
			.cart-item-number {
				border-left: 1px solid grey;
				border-right: 1px solid grey;
				text-align: center;
				width: 20px;
			}
			
			.cart-item-name {
			}
			
			.cart-item-quantity {
				border-left: 1px solid grey;
				border-right: 1px solid grey;
				text-align: center;
				width: 30px;
			}
			
			td.cart-item-price {
				border-left: 1px solid grey;
				border-right: 1px solid grey;
				width: 100px;
			}
			
			td.cart-total-label {
				text-align: right;
				border: 1px solid grey;
			}
			
			td.cart-total-price {
				border: 1px solid grey;
			}
			
			div#message-card {
				padding: 0;
				margin-top: 20px;
				margin-left: 0px;
				width: 300px;
				height: 200px;
				background: url('<?php print PUBLIC_URL."/images/flowercard4.png"; ?>') no-repeat;
			}
			
			div#message-card > div {
				position: relative;
				top: 17px;
				left: 20px;
				font-size: 9pt;
				font-family: georgia;
			}
			
			div#message-card > div > textarea {
				opacity: 0.6;
				margin-top: 10px;
				padding: 5px;
				border: 0;
				font-size: 12pt;
			}
			
			div.error {
				text-align: center;
				width: 280px;
				color: #db391e;
				background-color: #eaeaae;
				font-family: arial;
				font-size: 9pt;
				padding: 5px;
				margin: 0 auto;
				font-weight: bold;
			}
			
			.ui-widget {
				font-size: 0.7em;
			}
			
			#invoice {
				float: left;
				width: 680px;
				padding: 20px 0 50px 50px;
			}
			
			#invoice-table {
				width: 650px;
				font-size: 10pt;
			}
			
			#order-number {
				font-size: 13pt;
				font-weight: bold;
				text-decoration: underline;
			}
			
			#delivery-recipient {
				font-weight: bold;
			}
			
			#delivery-date {
				font-weight: bold;
			}
			
			#delivery-location {
				font-weight: bold;
				font-style: italic;
				text-decoration: underline;
			}
			
			p {
				line-height: 20px;
				font-size: 10pt;
			}
			
		</style>
	</head>
	<body>
	<div id="wrapper">
		<?php dispatch("Banner"); ?>
		<?php include_once(dirname(__FILE__)."/leftbar.php"); ?>
		
		<div id="content-box">
		
		<form action="<?php print $actionUrl; ?>" method="post">
		
			<?php if(isset($errors) && is_array($errors)) { ?>
				<div class="error">
				<?php 
					foreach($errors as $field => $msg) {
						print $msg . "<br/>";
					} 
				?>
				</div>
			<?php } ?>
			
			<div id="invoice">
			<table id="invoice-table">
				<tr>
					<td><h3>Thank you! Your order is complete.</h3></td>
				</tr>
				<tr>
					<td style="padding: 0 0 15px 0; font-size: 11pt;">Your order number is <span id="order-number"><?php print $delivery->order_number; ?></span></td>
				</tr>
				<tr>
					<td style="padding: 0 0 10px 0;">
					<p>
					Delivery will be made to <span id="delivery-recipient"><?php print $delivery->name; ?></span>
					at <span id="delivery-location"><?php print "{$delivery->address1}, " . ($delivery->address2 ? "{$delivery->address2}, " : "") . "{$delivery->zip} {$delivery->city}, {$delivery->state}"; ?></span> on
					<span id="delivery-date"><?php print date_format(date_create($delivery->delivery_date), "l, jS F"); ?></span>
					</p>
					<?php if(strlen($delivery->additional_instructions) > 0) { ?>
					<p>
					Additional instructions, <?php print $delivery->additional_instructions; ?>
					</p>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<td>
					<table class="cart-table">
						<thead>
						<tr class="cart-column-header">
							<th></th>
							<th>Item</th>
							<th>Qty</th>
							<th>Price</th>
						</tr>
						</thead>
						<tbody>
						<?php
						global $paths;
						$o = "";
						$totalPrice = 0;
						if($cartItems)
						foreach($cartItems as $i => $cartItem) {
							$number = $i + 1;
							$quantityPrice = $cartItem['price'] * $cartItem['quantity'];
							$totalPrice += $quantityPrice;
							$quantityPrice = number_format($quantityPrice, 2, '.', '');
							
							$itemName = ucwords(strtolower($cartItem['name']));
							
							$o .= <<<EOS
							<tr class="cart-item">
								<td class="cart-item-number">{$number}</td>
								<td class="cart-item-name">{$itemName} ({$cartItem['product_id']})</td>
								<td class="cart-item-quantity">{$cartItem['quantity']}</td>
								<td class="cart-item-price">{$cartItem['currency_prefix']} {$quantityPrice}</td>
							</tr>
EOS;
						}
						$totalPrice += $deliveryFee;
						$deliveryFee = number_format($deliveryFee, 2, '.', '');
						$totalPrice = number_format($totalPrice, 2, '.', '');
						$o .= <<<EOS
						<tr class="cart-item">
							<td colspan="3" class="cart-total-label">Delivery</td>
							<td class="cart-total-price">{$cartItems[0]['currency_prefix']} {$deliveryFee}</td>
						</tr>
						<tr class="cart-item">
							<td colspan="3" class="cart-total-label">Total</td>
							<td class="cart-total-price">{$cartItems[0]['currency_prefix']} {$totalPrice}</td>
						</tr>
EOS;
						print $o;
						
						?>
						</tbody>
					</table>
					</td>
				</tr>
				<tr>
					<td style="padding: 15px 0 0 0; font-size: 9pt;">You should be receiving an email with your order information at <strong><i><?php print $customerEmail; ?></i></strong></td>
				</tr>
			</table>
			</div>

		</form>
		</div>
		<?php include_once(dirname(__FILE__)."/footer.php"); ?>
	</div>
	</body>
</html>