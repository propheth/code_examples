<link href="<?php print PUBLIC_URL; ?>/css/dropdown.css" rel="stylesheet" type="text/css" />
<link href="<?php print PUBLIC_URL; ?>/css/dropdown.floweries.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-15469523-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<style type="text/css">

	#banner {
		position: relative;
		width: 100%;
		height: 105px;
		padding: 0px;
		background-color: #fdfddd;
		background: url('<?php print PUBLIC_URL . "/images/bannerbg4.png"; ?>') repeat-x;
		border-bottom: 1px solid #938282;
	}
	
	#banner-nav {
		position: relative;
		margin-top: -18.5px;
		width: 580px;
		height: 20px;
		margin-right: 0px;
		/*background: url('<?php print PUBLIC_URL . "/images/bannernav3.png"; ?>') no-repeat;*/
		float: right;
		z-index: 128;
	}
	
	.banner-item {
		display: inline;
		cursor: pointer;
		/*background-color: #380000;*/
		color: white;
		margin: 0 10px;
		font-family: trebuchet ms;
		font-weight: bold;
		font-size: 10pt;
	}
	
	/* css drop-down */

	ul.link
	{
		margin: 0;
		padding: 0;
		list-style: none;
	}

	li.link
	{
		position: relative;
		font-family: trebuchet ms;
		font-size: 10pt;
		font-weight: bold;
		padding-left: 20px;
		padding-right: 20px;
		float: left;
		color: white;
	}

	li.link a:link
	{
		text-decoration: none;
		color: white;
	}

	li.link a:visited
	{
		text-decoration: none;
		color: white;
	}

	li.link ul.sublink
	{
		position: absolute;
		margin: 0;
		padding: 0;
		list-style: none;
		border: 2px solid #380000;
		min-width: 100px;
		display: none;
		background-color: #f8ffdb;
		float: left;
		color: #380000;
	}

	li.link:hover ul.sublink
	{
		display: block;
		color: #380000;
	}
	
	li.sublink
	{
		position: relative;
		padding: 3px 5px;
		font-size: 9pt;
		background-color: #f8ffdb;
		min-width: 100px;
		color: #380000;
	}

	li.sublink a
	{
		display: block;
		min-width: 100%;
		color: #380000;
	}

	li.sublink a:link
	{
		color: #380000;
	}
	
	li.sublink a:visited
	{
		color: #380000;
	}
	
	li.sublink:hover
	{
		background-color: #d4e883;
		color: #380000;
	}

	li.sublink:hover a:link
	{
		text-decoration: none;
		color: #380000;
	}

	li.sublink:hover a:visited
	{
		text-decoration: none;
		color: #380000;
	}

	li.sublink ul.subsublink
	{
		margin-top: -23px;
		margin-left: 0;
		padding: 0;
		list-style: none;
		position: absolute;
		left: 110px;
		border: 2px solid #380000;
		min-width: 100px;
		background-color: white;
		display: none;
	}

	li.subsublink
	{
		position: relative;
		padding: 2px 5px;
		list-style-type: none;
		background-color: white;
		min-width: 100px;
	}

	li.sublink:hover ul.subsublink
	{
		display: block;
	}

	li.subsublink:hover
	{
		background-color: #d4e883;
	}
	
	.onHoverUnderline
	{
		text-decoration: none;
	}
	
	.onHoverUnderline:hover
	{
		text-decoration: underline;
	}
	
</style>

<div id="banner">
	<div>
		<img alt="Kosova Flower Logo" style="height: 97px; margin: 2.5px;" src="<?php print PUBLIC_URL . "/images/logo.png"; ?>" />
	</div>
	<!--
	<div style="float: right; display: none;">
		<form>
			<table>
				<tr>
					<td>Login:</td>
					<td><input name="username" type="text"/></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><input name="password" type="password"/></td>
				</tr>
			</table>
		</form>
	</div>
	-->
	<?php
		global $paths;
	?>
	<div id="banner-nav">
		<ul class="menu">
			<li class="menu-entry-final">
				<a href="<?php print $paths['REL_ROOT_URL']; ?>/">Home</a>
			</li>
			
			<!--
			<li class="menu-entry">Occasion&nbsp;&nbsp;&nbsp;&#9662;
				<ul class="submenu">
					<li class="submenu-entry">Birthday</li>
					<li class="submenu-entry">Love</li>
					<li class="submenu-entry">Anniversary</li>
					<li class="submenu-entry">Congratulations</li>
					<li class="submenu-entry">Wedding & Engagement</li>
					<li class="submenu-entry">Get well</li>
					<li class="submenu-entry">I'm sorry</li>
					<li class="submenu-entry">Funeral</li>
					<li class="submenu-entry">Thank you</li>
				</ul>
			</li>
			-->
			
			<li class="menu-entry">Flowers&nbsp;&nbsp;&nbsp;&#9662;
				<ul class="submenu">
					<li class="submenu-entry"><a href="<?php print REL_ROOT_URL; ?>/roses">Roses</a></li>
					<li class="submenu-entry"><a href="<?php print REL_ROOT_URL; ?>/daisies">Daisies</a></li>
					<li class="submenu-entry"><a href="<?php print REL_ROOT_URL; ?>/calla">Calla</a></li>
					<li class="submenu-entry"><a href="<?php print REL_ROOT_URL; ?>/mixed">Mixed</a></li>
				</ul>
			</li>
			
			<li class="menu-entry">Info
				<ul class="submenu">
					<li class="submenu-entry"><a href="<?php print $paths['REL_ROOT_URL']; ?>/info#delivery">Delivery</a></li>
					<li class="submenu-entry"><a href="<?php print $paths['REL_ROOT_URL']; ?>/info#payment">Payment</a></li>
					<li class="submenu-entry"><a href="<?php print $paths['REL_ROOT_URL']; ?>/info#substitution-policy">Substitution Policy</a></li>
					<li class="submenu-entry"><a href="<?php print $paths['REL_ROOT_URL']; ?>/info#cancellation-policy">Cancellation Policy</a></li>
					<li class="submenu-entry"><a href="<?php print $paths['REL_ROOT_URL']; ?>/about-us#partnership">Partnership</a></li>
					<li class="submenu-entry"><a href="<?php print $paths['REL_ROOT_URL']; ?>/about-us#about">About Us</a></li>
				</ul>
			</li>
			
			<li class="menu-entry">Support
				<ul class="submenu">
					<li class="submenu-entry"><a href="<?php print $paths['REL_ROOT_URL']; ?>/contact-us">Contact Us</a></li>
				</ul>
			</li>
		</ul>
	</div>
	<?php
		$cartItemCount = $model;
	?>
	<div>
		<li style="position: absolute; list-style: none; top: 45px; right: 100px; font-family: trebuchet ms; font-size: 9pt; font-weight: bold;">
			<table><tr>
				<td><img alt="shopping cart" src="<?php print $paths['PUBLIC_URL']; ?>/images/carticonbk.png" style="width: 30px;" /></td>
				<td><a style="color: #232323; padding: 3px;" class="onHoverUnderline" href="<?php print $paths['REL_ROOT_URL']; ?>/cart"><?php if(!empty($cartItemCount)) { ?><?php print $cartItemCount; ?><?php } else { ?>0<?php } ?> item(s) in cart</a></td>
			</tr></table>
		</li>
	</div>
</div>