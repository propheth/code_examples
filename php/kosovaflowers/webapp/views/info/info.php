<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Information - Payment, Delivery, Substitution, Cancellation Policy</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="Kosova Flowers is the premier flower, chocolate and gift delivery service in Kosova. We try to make your ordering process simple and worry free to anywhere we deliver around Kosovo, Pristina and Ferizaj. Our prices are cheaper than the others and arrangements beautiful." />
		<meta name="keywords" content="kosova, kosovo, pristina, ferizaj, flower, flowers, chocolate, gift, gifts, delivery" />
		<link href="<?php print PUBLIC_URL; ?>/css/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="<?php print PUBLIC_URL; ?>/css/default.css" rel="stylesheet" type="text/css" />
		<script src="<?php print PUBLIC_URL; ?>/jquery/jquery-1.4.4.js"></script>
		<script type="text/javascript">
			$(function(){

			});
		</script>
		<style type="text/css">
			
			div {
				/*border: 1px solid red;*/
			}
			
			h3 {
				position: relative;
				left: 3em;
				font-size: 10pt;
				text-decoration: underline;
				color: #4d1a03;
			}
			
			h3 > a {
				font-size: 10pt;
				text-decoration: underline;
				margin: 20px 0;
				color: #4d1a03;
			}
			
		</style>
	</head>
	
	<body>
	<div id="wrapper">
		<?php dispatch("Banner"); ?>
		<?php include_once(dirname(__FILE__)."/../leftbar.php"); ?>
		
		<div id="content-box">
		
		<h3><a name="delivery">Delivery</a></h3>
		
		<p class="text1">
		We take special care to ensure that your order gets delivered on time,
		with the design chosen and the freshest flowers possible.
		</p>
		
		<ul class="text1">
			<li>We will try our best to deliver your order your chosen date,
			given all delivery information is correct.</li>
			<li>Before delivery, we will call the recipient to make the
			necessary delivery arrangements.</li>
			<li>If we fail to get in touch with the recipient we will still
			make the delivery on your chosen date.</li>
			<li>We will make every effort to find the recipient. However, 
			refunds will not be given if the recipient can't be found at
			the location given after every effort is put forward to reach
			the recipient.</li>
		</ul>
		
		<p class="text1">
		<strong>Do not worry, we will make every effort to get your order to
		the recipient.</strong>
		</p>
		
		<h3><a name="payment">Payment</a></h3>
		
		<p class="text1">
		Rest assured your payment information is safe with us. We do not store
		any of your payment credentials. All purchases go through independant
		payment service providers.
		</p>
		
		<p class="text1">
		We use Paypal and 2Checkout as our payment providers. All payment
		credentials entered are over 128-bit SSL at our payment service
		provider's page.
		</p>
		
		<h3><a name="substitution-policy">Substitution Policy</a></h3>

		<p class="text1">
		We will try out best to deliver the exact ordered flowers, arrangement
		and gifts. But depending on season and availability, we have the right
		to change the colour and kind of ordered flowers and gifts with one of
		equal or greater value.
		</p>
		
		<h3><a name="cancellation-policy">Cancellation Policy</a></h3>
		
		<p class="text1">
		If for any reason you wish to change or cancel your order, please
		notify us as soon as possible by email.<br/>
		<br/>
		We are happy to accommodate requests for changes or cancellations as
		long as we receive them before the order has been prepared for
		delivery.<br/>
		<br/>
		48 hour advance notice before the delivery date is necessary to assure
		a cancellation or any changes to your order. An order cannot be 
		cancelled or changed after it has been prepared for delivery.<br/>
		<br/>
		You should receive any order refunds in a few days on your card or
		the method you paid, depending on the payment service provider.
		</p>
		
		</div>
		
		<?php include_once(dirname(__FILE__)."/../footer.php"); ?>
	</div>
	</body>
	
</html>