<?php
	
	$item = $model;
	$actionUrl = formAction("cart");
	$itemPrice = number_format($item['price'], 2, '.', '');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title><?php print $item['name']; ?> - <?php print $item['short_description']; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="Kosova Flowers is the premier flower, chocolate and gift delivery service in Kosova. We try to make your ordering process simple and worry free to anywhere we deliver around Kosovo, Pristina and Ferizaj. Our prices are cheaper than the others and arrangements beautiful." />
		<meta name="keywords" content="kosova, kosovo, pristina, ferizaj, flower, flowers, chocolate, gift, gifts, delivery" />
		<link href="<?php print PUBLIC_URL; ?>/css/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="<?php print PUBLIC_URL; ?>/css/default.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="<?php print PUBLIC_URL; ?>/jquery/lightbox/js/jquery.js"></script>
		<script type="text/javascript" src="<?php print PUBLIC_URL; ?>/jquery/lightbox/js/jquery.lightbox-0.5.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php print PUBLIC_URL; ?>/jquery/lightbox/css/jquery.lightbox-0.5.css" media="screen" />
		<script type="text/javascript">
			$(function() {
				$('.item-image-default a').lightBox();
			});
		</script>
		<style type="text/css">
			
			div {
				/*border: 1px solid red;*/
			}
			
			.item-placeholder {
				padding: 10px 0;
				vertical-align: top;
			}
			
			.item-image-default {
				position: relative;
				top: 20px;
				text-align: center;
				vertical-align: middle;
				width: 340px;
				float: left;
			}
			
			.item-image-default img {
				max-width: 330px;
				max-height: 380px;
				/*
				-webkit-box-shadow:8px 8px 12px #4d4848;
				-moz-box-shadow: 8px 8px 12px #4d4848;
				*/
			}
			
			.item-info {
				float: left;
				width: 300px;
				margin: 0 10px;
				padding: 0 5px;
			}
			
			.item-title {
				margin-top: 10px;
				font-size: 13pt;
				font-weight: bold;
				color: #380000;
			}
			
			.item-productid {
				font-size: 9pt;
				font-style: italic;
				color: #7f6229;
			}
			
			.item-short-desc {
				font-size: 10pt;
				margin: 10px 0;
				color: #111111;
			}
			
			.item-long-desc {
				font-size: 10pt;
				margin: 10px 0;
				color: #111111;
				text-align: justify;
			}
			
			.item-price {
				font-size: 12pt;
				font-weight: bold;
				font-style: italic;
				margin: 20px 0;
				width: 100px;
				float: left;
				color: #2e514f;
			}
			
			.item-add-to-cart {
				float: left;
				margin: 15px 0;
			}
			
			.item-add-to-cart input[type=submit] {
				font-size: 8pt;
				padding: 4px;
				font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
				width: 80px;
			}
			
			#content-box {
				position: relative;
				left: 0px;
				width: 100%;
				padding: 0;
				margin: 0;
			}
			
			#item-box {
				width: auto;
				margin-left: 180px;
				margin-top: 25px;
			}
			
			
		</style>
	</head>
	
	<body>
	<div id="wrapper">
		<?php dispatch("Banner"); ?>
		<?php include_once(dirname(__FILE__)."/leftbar.php"); ?>
		<div id="content-box">
			<div id="item-box">
				<form action="<?php print $actionUrl; ?>" method="post">
				<div class="item-info">
					<div class="item-title"><?php print $item['name']; ?></div>
					<div class="item-productid">Product Id: <?php print $item['product_id']; ?></div>
					<div class="item-short-desc"><?php print $item['short_description']; ?></div>
					<div class="item-long-desc"><?php print $item['long_description']; ?></div>
					<div class="item-price"><?php print "{$item['currency_prefix']} {$itemPrice}"; ?></div>
					<div class="item-add-to-cart"><input name="add" type="submit" value="Add to cart"/></div>
				</div>
				<div class="item-image-default">
					<a href="<?php print $item['image_default_path']; ?>" alt="<?php print $item['name']; ?>" title="<?php print $item['short_description']; ?>">
						<img src="<?php print $item['image_default_path']; ?>"/>
					</a>
				</div>
				<input name="do" type="hidden" value="add"/>
				<input name="itemId" type="hidden" value="<?php print $item['id']; ?>"/>
				</form>
			</div>
		</div>
		<?php include_once(dirname(__FILE__)."/footer.php"); ?>
	</div>
	</body>
	
</html>