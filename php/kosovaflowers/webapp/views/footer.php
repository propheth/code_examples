<?php
	global $paths;
?>
<style type="text/css">
	#footer { 
		color: white;
		margin-top: 30px;
		margin-bottom: 10px;
		padding-top: 5px;
		font-style: italic;
		clear: both;
		border-top: 1px solid #575a47;
		background: url('<?php print PUBLIC_URL . "/images/footerbg.png"; ?>') repeat-x;
		/*background-color: #2f2525;*/
		/*background-color: rgba(87, 90, 71, 1.0);*/
		height: 80px;
	}
	
	#footer #links {
		color: white;
		font-size: 8pt;
		width: 600px;
		margin: 10px auto;
		text-align: center;
	}
	
	#footer #links li {
		display: inline;
	}
	
	#footer #links a {
		color: white;
		font-style: normal;
		font-weight: bold;
		text-decoration: normal;
	}
</style>

<div id="footer">
	
	<div style="float: left; margin-top: 10px; margin-left: 17px; width: 135px;">
		<div style="margin-bottom: 3px; font-size: 8pt;">Powered by</div>
		<a href="http://www.2checkout.com"><img style="height: 35px;" alt="2checkout logo" src="<?php print PUBLIC_URL; ?>/images/2checkoutlogo.png"></a>
	</div>
	<div style="float: left; margin-top: 7px;">
		<div id="links">
			<ul>
				<li></li>
				<li><a href="<?php print $paths['REL_ROOT_URL']; ?>/info#delivery">Delivery</a></li> |
				<li><a href="<?php print $paths['REL_ROOT_URL']; ?>/info#payment">Payment</a></li> |
				<li><a href="<?php print $paths['REL_ROOT_URL']; ?>/info#substitution-policy">Substitution Policy</a></li> |
				<li><a href="<?php print $paths['REL_ROOT_URL']; ?>/info#cancellation-policy">Cancellation Policy</a></li> |
				<li><a href="<?php print $paths['REL_ROOT_URL']; ?>/about-us#partnership">Partnership</a></li> |
				<li><a href="<?php print $paths['REL_ROOT_URL']; ?>/about-us">About Us</a></li> |
				<li><a href="<?php print $paths['REL_ROOT_URL']; ?>/contact-us">Contact Us</a></li>
			</ul>
		</div>
		<div style="text-align: center; font-size: 8pt;">2010 Kosova Flowers</div>
	</div>
</div>